﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.RestSDK
{
     public class Configuration
    {
        public string ServiceURL { get; set; }
        public int RequestTimeout { get; set; }
        public bool AllowDeletions { get; set; }
        public int ErrorRetryMax { get; set; }
        public int ErrorRetryDelay { get; set; }


        public Configuration(string serviceUrl)
        {
            AllowDeletions = true;
            RequestTimeout = 100000;
            ServiceURL = serviceUrl;
            ErrorRetryMax = 5;
            ErrorRetryDelay = 60000;
        }


        internal void AreConfigurationSet()
        {
            if (ServiceURL == null)
                throw new ArgumentNullException("ServiceURL", "ServiceURL cannot be null.");

        }
    }
}
