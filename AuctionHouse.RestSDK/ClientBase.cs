﻿using AuctionHouse.RestSDK.Interfaces;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.RestSDK
{
    public abstract class ClientBase
    {
        private readonly Configuration _Configuration;

        protected ClientBase(Configuration _configuration)
        {
            _configuration.AreConfigurationSet();
            _Configuration = _configuration;
        }

        protected IClientResponse<T> GetData<T>(string resourceEndpoint)
            where T : new()
        {
            return GetData<T>(resourceEndpoint, null);
        }

        protected IClientResponse<T> GetData<T>(string resourceEndpoint, IFilter filter)
            where T : new()
        {
            var request = new RestRequest(resourceEndpoint);

            if (filter != null)
            {
                filter.AddFilter(request);
            }

            var response = RestGet<T>(request);

            var clientResponse = new ClientResponse<T>()
            {
                RestResponse = response,
            };

            var deserializedObject = new NewtonSoftJsonDeserializer().Deserialize<T>(response);

            if (response.Data != null)
            {
                clientResponse.Data = deserializedObject;
            }            
            return (IClientResponse<T>)clientResponse;
        }

        protected IClientResponse<T> PutData<T>(string resourceEndpoint, string json)
            where T : new()
        {
            var request = new RestRequest(resourceEndpoint);

            request.AddParameter("application/json", json, ParameterType.RequestBody);

            var response = RestPut<T>(request);

            var clientResponse = new ClientResponse<T>()
            {
                RestResponse = response,
                Data = response.Data
            };
        
            return clientResponse;
        }

        protected IClientResponse<T> PostData<T>(string resourceEndpoint, string json)
            where T : new()
        {
            var request = new RestRequest(resourceEndpoint);

            request.AddParameter("application/json", json, ParameterType.RequestBody);

            var response = RestPost<T>(request);

            var clientResponse = new ClientResponse<T>()
            {
                RestResponse = response,
                Data = response.Data
            };

            return clientResponse;
        }
        protected IClientResponse<bool> DeleteData(string resourceEndpoint)
        {

            IClientResponse<bool> clientResponse = null;
          
            if (_Configuration.AllowDeletions)
            {

                var request = new RestRequest(resourceEndpoint);

                var response = RestDelete<object>(request);

                clientResponse = new ClientResponse<bool>()
                {
                    RestResponse = response,
                    Data = response.StatusCode == System.Net.HttpStatusCode.NoContent ? true : false
                };

            }
            else
            {
                clientResponse = new ClientResponse<bool>()
                {
                    RestResponse = null,
                    Data = false
                };
            }          
            return clientResponse;
        }     

        private IRestResponse<T> RestGet<T>(IRestRequest request) where T : new()
        {
            request.Method = Method.GET;

            var client = new RestClient(_Configuration.ServiceURL);

            var response = RestExecute<T>(request, client);

            return response;
        }
        private IRestResponse<T> RestPut<T>(IRestRequest request) where T : new()
        {
            request.Method = Method.PUT;

            var client = new RestClient(_Configuration.ServiceURL);

            var response = RestExecute<T>(request, client);

            return response;
        }
        private IRestResponse<T> RestPost<T>(IRestRequest request) where T : new()
        {
            request.Method = Method.POST;

            var client = new RestClient(_Configuration.ServiceURL);

            var response = RestExecute<T>(request, client);

            return response;
        }
        private IRestResponse<T> RestDelete<T>(IRestRequest request) where T : new()
        {
            request.Method = Method.DELETE;

            var client = new RestClient(_Configuration.ServiceURL);

            var response = RestExecute<T>(request, client);

            return response;
        }
        private IRestResponse<T> RestOptions<T>(IRestRequest request) where T : new()
        {
            request.Method = Method.OPTIONS;

            var client = new RestClient(_Configuration.ServiceURL);

            var response = RestExecute<T>(request, client);

            return response;
        }



        private IRestResponse<T> RestExecute<T>(IRestRequest request, IRestClient restClient) where T : new()
        {

            request.RequestFormat = DataFormat.Json;
            request.AddParameter("Accept", "application/json", ParameterType.HttpHeader);         


            ((RestClient)restClient).AddHandler("application/json", new NewtonSoftJsonDeserializer());

            var client = restClient;          
            client.Timeout = _Configuration.RequestTimeout;

            var response = client.Execute<T>(request);
          
            return response;
        }      
    }
}
