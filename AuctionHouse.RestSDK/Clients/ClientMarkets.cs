﻿using AuctionHouse.RestSDK.DTO;
using AuctionHouse.RestSDK.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.RestSDK.Clients
{
   public  class ClientMarkets: ClientBase
        //IParentResourceGetUpdateDelete<Market>

    {
        public ClientMarkets(Configuration configuration)
            :base(configuration)
        { }

        public IClientResponse<List<MarketDTO>> Get()
        {
            string resourceEndpoint = "/api/markets";
            return base.GetData<List<MarketDTO>>(resourceEndpoint);
        }

        public IClientResponse<List<ProductDTO>> GetProducts(int id)
        {
            string resourceEndpoint = string.Format("/api/market/{0}/products", id);
            return base.GetData<List<ProductDTO>>(resourceEndpoint);
        }

        public IClientResponse<MarketDTO> Create(string json)
        {
            string resourceEndpoint = "/api/market/create";
            return base.PostData<MarketDTO>(resourceEndpoint, json);
        }

        public IClientResponse<bool> Delete(int id)
        {
            string resourceEndpoint = string.Format("/api/market/delete/{0}", id);
            return base.DeleteData(resourceEndpoint);
        }


        //public IClientResponse<List<MarketDTO>> GetUserMarket(string name)
        //{
        //    string resourceEndpoint = string.Format("api/markets/{0}", name);
        //    return base.GetData<List<MarketDTO>>(resourceEndpoint);
        //}

    }
}
