﻿using AuctionHouse.RestSDK.DTO;
using AuctionHouse.RestSDK.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.RestSDK.Clients
{
    public class ClientOrderLInes : ClientBase
    {
        public ClientOrderLInes(Configuration configuration)
            : base(configuration)
        { }
        public IClientResponse<OrderLineDTO> Create(int productId)
        {
            string resourceEndpoint = string.Format("/api/orderline/create/{0}", productId);
            return base.GetData<OrderLineDTO>(resourceEndpoint);
        }
    }
}
