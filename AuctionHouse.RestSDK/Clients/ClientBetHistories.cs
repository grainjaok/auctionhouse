﻿using AuctionHouse.RestSDK.DTO;
using AuctionHouse.RestSDK.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.RestSDK.Clients
{
    public class ClientBetHistories : ClientBase
    {
        public ClientBetHistories(Configuration configuration)
            : base(configuration)
        { }

        public IClientResponse<BetHistoryDTO> Create(string json)
        {
            string resourceEndpoint = "/api/bethistory/create";
            return base.PostData<BetHistoryDTO>(resourceEndpoint, json);
        }
    }
}
