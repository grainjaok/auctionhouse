﻿using AuctionHouse.RestSDK.DTO;
using AuctionHouse.RestSDK.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.RestSDK.Clients
{
   public class ClientProducts:ClientBase
    {
        public ClientProducts(Configuration configuration)
            :base(configuration)
        { }

        public IClientResponse<ProductDTO> Create(string json)
        {
            string resourceEndpoint = "/api/product/create";
            return base.PostData<ProductDTO>(resourceEndpoint, json);
        }

        public IClientResponse<List<ProductDTO>> CreateMany(string json)
        {
            string resourceEndpoint = "/api/product/createmany";
            return base.PostData<List<ProductDTO>>(resourceEndpoint, json);
        }

        public IClientResponse<List<ProductDTO>> Get()
        {
            string resourceEndpoint = "/api/products";
            return base.GetData<List<ProductDTO>>(resourceEndpoint);
        }

        public IClientResponse<bool> Delete(int id)
        {
            string resourceEndpoint = string.Format("/api/product/delete/{0}", id);
            return base.DeleteData(resourceEndpoint);
        }

        public IClientResponse<ProductDTO> GetById(int id)
        {
            string resourceEndpoint = string.Format("/api/product/{0}", id);
            return base.GetData<ProductDTO>(resourceEndpoint);
        }

        public IClientResponse<ProductDTO> Update(int id, string json)
        {
            string resourceEndpoint = string.Format("/api/product/edit/{0}", id);
            return base.PutData<ProductDTO>(resourceEndpoint, json);
        }
    }
}
