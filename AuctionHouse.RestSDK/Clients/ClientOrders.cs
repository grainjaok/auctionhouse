﻿using AuctionHouse.RestSDK.DTO;
using AuctionHouse.RestSDK.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.RestSDK.Clients
{
    public class ClientOrders : ClientBase
    {
        public ClientOrders(Configuration configuration)
            : base(configuration)
        { }
        public IClientResponse<List<OrderDTO>> GetMarketOwnerOrders(int id)
        {
            string resourceEndpoint = string.Format("/api/order/marketownerorders/{0}", id);
            return base.GetData<List<OrderDTO>>(resourceEndpoint);
        }

        public IClientResponse<bool> AcceptOrder(int id)
        {
            string resourceEndpoint = string.Format("/api/order/{0}/accept", id);
            return base.GetData<bool>(resourceEndpoint);
        }
    }
}
