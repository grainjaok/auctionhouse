﻿using AuctionHouse.RestSDK.DTO;
using AuctionHouse.RestSDK.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.RestSDK.Clients
{
    public class ClientCategories : ClientBase
    {
        public ClientCategories(Configuration configuration)
            : base(configuration)
        { }
        public IClientResponse<CategoryDTO> Create(string json)
        {
            string resourceEndpoint = "/api/category/create";
            return base.PostData<CategoryDTO>(resourceEndpoint, json);
        }

        public IClientResponse<List<CategoryDTO>> GetAll()
        {
            string resourceEndpoint = "/api/categories";
            return base.GetData<List<CategoryDTO>>(resourceEndpoint);
        }

        public IClientResponse<List<ProductDTO>> GetProducts(int id)
        {            
            string resourceEndpoint = string.Format("/api/category/{0}/products", id);
            return base.GetData<List<ProductDTO>>(resourceEndpoint);
        }
    }
}
