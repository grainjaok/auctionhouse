﻿using AuctionHouse.RestSDK.DTO;
using AuctionHouse.RestSDK.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.RestSDK.Clients
{
  public  class ClientPaymentMethods:ClientBase
    {
        public ClientPaymentMethods(Configuration configuration)
            : base(configuration)
        { }
        public IClientResponse<PaymentMethodDTO> Create(string json)
        {
            string resourceEndpoint = "/api/paymentmethod/create";
            return base.PostData<PaymentMethodDTO>(resourceEndpoint, json);
        }

        public IClientResponse<List<PaymentMethodDTO>> GetAll()
        {
            string resourceEndpoint = "/api/paymentmethods";
            return base.GetData<List<PaymentMethodDTO>>(resourceEndpoint);
        }
    }
}
