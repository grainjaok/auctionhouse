﻿using AuctionHouse.RestSDK.DTO;
using AuctionHouse.RestSDK.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.RestSDK.Clients
{
   public class ClientCurrencies :ClientBase
    {
        public ClientCurrencies(Configuration configuration)
            : base(configuration)
        { }

        public IClientResponse<CurrencyDTO> GetCurrency()
        {
            string resourceEndpoint = "/api/currency";
            return base.GetData<CurrencyDTO>(resourceEndpoint);
        }
    }
}
