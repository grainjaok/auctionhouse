﻿using AuctionHouse.RestSDK.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.RestSDK
{

    public class Client
    {
        private readonly Configuration _Configuration;

        public Client(Configuration configuration)
        {
            _Configuration = configuration;
        }

        private ClientMarkets _Markets;
        public ClientMarkets Markets
        {
            get
            {
                if (_Markets == null)
                    _Markets = new ClientMarkets(_Configuration);
                return _Markets;
            }
        }

        private ClientStorages _Storages;
        public ClientStorages Storages
        {
            get
            {
                if (_Storages == null)
                    _Storages = new ClientStorages(_Configuration);
                return _Storages;
            }
        }

        private ClientCategories _Categories;
        public ClientCategories Categories
        {
            get
            {
                if (_Categories == null)
                    _Categories = new ClientCategories(_Configuration);
                return _Categories;
            }
        }

        private ClientProducts _Products;
        public ClientProducts Products
        {
            get
            {
                if (_Products == null)
                    _Products = new ClientProducts(_Configuration);
                return _Products;
            }
        }

        private ClientPaymentMethods _PaymentMethods;
        public ClientPaymentMethods PaymentMethods
        {
            get
            {
                if (_PaymentMethods == null)
                    _PaymentMethods = new ClientPaymentMethods(_Configuration);
                return _PaymentMethods;
            }
        }

        private ClientBetHistories _ClientBetHistories;
        public ClientBetHistories ClientBetHistories
        {
            get
            {
                if (_ClientBetHistories == null)
                    _ClientBetHistories = new ClientBetHistories(_Configuration);
                return _ClientBetHistories;
            }
        }

        private ClientOrderLInes _ClientOrderLInes;
        public ClientOrderLInes ClientOrderLInes
        {
            get
            {
                if (_ClientOrderLInes == null)
                    _ClientOrderLInes = new ClientOrderLInes(_Configuration);
                return _ClientOrderLInes;
            }
        }

        private ClientOrders _ClientOrders;
        public ClientOrders ClientOrders
        {
            get
            {
                if (_ClientOrders == null)
                    _ClientOrders = new ClientOrders(_Configuration);
                return _ClientOrders;
            }
        }
      
    }
}
