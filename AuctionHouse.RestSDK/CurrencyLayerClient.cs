﻿using AuctionHouse.Domen.Helpers;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.RestSDK
{
   public static class CurrencyLayerClient
    {     
        public static CurrencyLayerModel GetApiRequest()
        {
            var client = new RestClient("http://apilayer.net");
            var request = new RestRequest("api/live?access_key=d731e197895ac5064e98e102c71697f0", Method.GET);
            CurrencyLayerModel currencyTOReturn = client.Execute<CurrencyLayerModel>(request).Data;
            return currencyTOReturn;
        }
    }
}
