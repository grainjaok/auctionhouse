﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.RestSDK.Interfaces
{
    public interface IClientResponse<T>
    {
        T Data { get; set; }
        global::RestSharp.IRestResponse RestResponse { get; set; }       
    }
}
