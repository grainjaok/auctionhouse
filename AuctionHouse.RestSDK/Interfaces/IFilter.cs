﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.RestSDK.Interfaces
{
    public interface IFilter
    {
        void AddFilter(IRestRequest request);
        int? Limit { get; set; }
        int? Page { get; set; }
    }
}
