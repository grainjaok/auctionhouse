﻿using AuctionHouse.Domen.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.RestSDK.DTO
{
    public class OrderDTO
    {
        public int Id { get; set; }

        public string DeliveryAdress { get; set; }

        public string PaymentMethod { get; set; }

        public string CustomerId { get; set; }
      
        public virtual ApplicationUser Customer { get; set; }

        public int StatusId { get; set; }
  
        public virtual OrderStatus OrderStatus { get; set; }

        public int OrderLineId { get; set; }
        
        public virtual OrderLine OrderLines { get; set; }
    }
}
