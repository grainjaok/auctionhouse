﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.RestSDK.DTO
{
    public class StorageDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Anddress { get; set; }
    }
}
