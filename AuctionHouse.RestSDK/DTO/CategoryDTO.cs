﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.RestSDK.DTO
{
    public class CategoryDTO
    {
        public int Id { get; set; }
        public string CategoryType { get; set; }
        public int ParentId { get; set; }
    }
}
