﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.RestSDK.DTO
{
   public class CurrencyDTO
    {
        public decimal EUR { get; set; }
        public decimal UAH { get; set; }
    }
}
