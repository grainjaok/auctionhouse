﻿using AuctionHouse.Domen.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.RestSDK.DTO
{
    public class OrderLineDTO
    {
        public int ProductId { get; set; }
       
        public virtual Product Product { get; set; }

        public int OrderId { get; set; }
      
        public virtual Order Order { get; set; }

        public int NumberOfProducts { get; set; }

        public decimal CurrentBet { get; set; }
    }
}
