﻿using AuctionHouse.Domen.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.RestSDK.DTO
{
    public class ProductDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string ImageUrl { get; set; }

        public DateTime AuctionEndDate { get; set; }

        public decimal MinBet { get; set; }

        public decimal BuyOut { get; set; }

        public string Description { get; set; }

        public int Number { get; set; }

        public decimal CurrentBet { get; set; }

        public int CategoryId { get; set; }
      
        public virtual Category Category { get; set; }

        public int MarketId { get; set; }

        public virtual Market Market { get; set; }

        public int StorageId { get; set; }
    
        public virtual Storage Storage { get; set; }

        public int PaymentMethodId { get; set; }

        public virtual PaymentMethod PaymentMethod { get; set; }

    }
}
