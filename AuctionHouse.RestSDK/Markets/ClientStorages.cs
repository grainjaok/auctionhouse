﻿using AuctionHouse.RestSDK.DTO;
using AuctionHouse.RestSDK.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.RestSDK.Markets
{
    public class ClientStorages: ClientBase
    {
        public ClientStorages(Configuration configuration)
            :base(configuration)
        { }
        public IClientResponse<StorageDTO> Create(string json)
        {
            string resourceEndpoint = "/api/storage/create";
            return base.PostData<StorageDTO>(resourceEndpoint, json);
        }
    }
}
