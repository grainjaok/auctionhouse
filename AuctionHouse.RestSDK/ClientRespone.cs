﻿using AuctionHouse.RestSDK.Interfaces;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.RestSDK
{
    public class ClientResponse<T> :IClientResponse<T>
    {
        public IRestResponse RestResponse { get; set; }
        public T Data { get; set; }
    }
}
