//[assembly: WebActivator.PostApplicationStartMethod(typeof(AuctionHouse.API.App_Start.SimpleInjectorWebApiInitializer), "Initialize")]

namespace AuctionHouse.API.App_Start
{
    using System.Web.Http;
    using SimpleInjector;
    using SimpleInjector.Integration.WebApi;
    using Domen.Interfaces;
    using BLL.Services;
    using DAL.Repositories;
    using Quartz.Impl;
    using Quartz.Spi;
    using Quartz;
    using OrderCreation;
    using SimpleInjector.Lifestyles;
    using System;
    using System.Linq;


    public static class SimpleInjectorWebApiInitializer
    {

        public static Container container { get; private set; }
     
        /// <summary>Initialize the container and register it as Web API Dependency Resolver.</summary>
        public static void Initialize()
        {
            container = new Container();

            container.Options.DefaultScopedLifestyle = new WebApiRequestLifestyle();

            InitializeContainer(container);

            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);
       
            container.Verify();

            GlobalConfiguration.Configuration.DependencyResolver =
                new SimpleInjectorWebApiDependencyResolver(container);
        }
     
        public static void InitializeContainer(Container container)
        {
          
            container.Register<IUnitOfWork, UnitOfWork>(Lifestyle.Singleton);

            container.Register<IOrderService, OrderService>(Lifestyle.Transient);

            container.Register<IProductsService, ProductService>(Lifestyle.Transient);

            container.Register<IBetHistoryService, BetHistoryService>(Lifestyle.Transient);

            container.Register<IStorageService, StorageService>(Lifestyle.Transient);

            container.Register<IUserService, UserService>(Lifestyle.Transient);

            container.Register<IMarketService, MarketService>(Lifestyle.Transient);

            container.Register<ICategoryService, CategoryService>(Lifestyle.Transient);         

            container.Register<IPaymentMethodService, PaymentMethodService>(Lifestyle.Transient);

            container.Register<IOrderStatusService, OrderStatusService>(Lifestyle.Transient);

            container.Register<IOrderLineService, OrderLineService>(Lifestyle.Transient);


            container.Register(() => new OrderScheduler(container));

            var jobTypes = AppDomain.CurrentDomain.GetAssemblies().ToList()
                    .SelectMany(s => s.GetTypes())
                    .Where(p => typeof(IJob).IsAssignableFrom(p) && !p.IsInterface);

            foreach (Type jobType in jobTypes)
            {
                container.Register(jobType, jobType);

            }




        }
        
    }
   
}