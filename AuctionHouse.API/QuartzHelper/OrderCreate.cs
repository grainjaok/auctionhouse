﻿using AuctionHouse.Domen.Entities;
using AuctionHouse.Domen.Helpers;
using AuctionHouse.Domen.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuctionHouse.API.OrderCreation
{
    public static class OrderCreate
    {
        public static void CreateOrder(Product product, IOrderService orderSerivce, IOrderStatusService orderStatusService ,BetHistory winningBet, IProductsService productService)
        {

            Order order = new Order
            {
                CustomerId = winningBet.UserId,
                DeliveryAdress = winningBet.User.Address,
                StatusId = OrderStatusHelper.StatusNew(orderStatusService).Id,
                PaymentMethod = product.PaymentMethod.MethodType,

                OrderLines = new OrderLine
                {
                    NumberOfProducts = 1,
                    ProductId = product.Id,
                    CurrentBet = winningBet.Bet
                }
            };
            product.Number--;
            productService.EditProduct(product);
            orderSerivce.CreateOrder(order);
        }
    }
}