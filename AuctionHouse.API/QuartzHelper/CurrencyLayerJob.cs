﻿using AuctionHouse.Domen.Helpers;
using AuctionHouse.RestSDK;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuctionHouse.API.QuartzHelper
{
    public class CurrencyLayerJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var currency = CurrencyLayerClient.GetApiRequest();
                CurrencyLayerHelper.SetNewCurrancy(currency);
            }
            catch (Exception e)
            {

                //  throw;
            }


        }
    }
}