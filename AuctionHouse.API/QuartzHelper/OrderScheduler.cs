﻿using Quartz;
using Quartz.Impl;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuctionHouse.API.OrderCreation
{
    public class OrderScheduler
    {
        private readonly Container _container;

        private ISchedulerFactory schedFact;

        private IScheduler sched;


        public OrderScheduler(Container container)
        {
            _container = container;
            this.schedFact = new StdSchedulerFactory();
            this.sched = this.schedFact.GetScheduler();
        }


        public void Start()
        {
            try
            {      
                this.sched.JobFactory = new StructureMapJobFactory(this._container);
                this.sched.Start();    

                IJobDetail orderJob = JobBuilder.Create<JobOrderCreator>().Build();

                ITrigger orderTrigger = TriggerBuilder.Create()
                    .WithIdentity("trigger1", "group1").StartNow()
                    .WithSimpleSchedule(x => x
                    .WithIntervalInMinutes(10)
                    .RepeatForever())
                    .Build();
              
                this.sched.ScheduleJob(orderJob, orderTrigger);
              

            }
            catch (Exception e)
            {
            
                //throw;
            } 

        }
    }
}