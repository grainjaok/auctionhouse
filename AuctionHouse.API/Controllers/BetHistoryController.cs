﻿using AuctionHouse.Domen.Entities;
using AuctionHouse.Domen.Interfaces;
using AuctionHouse.RestSDK.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AuctionHouse.API.Controllers
{
    public class BetHistoryController : ApiController
    {
        IBetHistoryService _betHistoryService;
        NLog.Logger logger;

        public BetHistoryController(IBetHistoryService betHistoryService)
        {
            _betHistoryService = betHistoryService;
            logger = NLog.LogManager.GetCurrentClassLogger();
        }


        [HttpPost]
        [Route("api/bethistory/create")]
        public IHttpActionResult CreateBetHistory(BetHistoryDTO betHistory)
        {
            try
            {
                BetHistory historyToCreate = new BetHistory {
                    Bet = betHistory.Bet,
                    ProductId = betHistory.ProductId,
                    UserId = betHistory.UserId
                };
                _betHistoryService.CreateBetHistory(historyToCreate);
                return Ok();
            }
            catch (Exception e)
            {
                logger.Fatal(e.Message);
            }
            return BadRequest();
        }
    }
}
