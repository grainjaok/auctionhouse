﻿using AuctionHouse.Domen.Helpers;
using AuctionHouse.Domen.Interfaces;
using AuctionHouse.RestSDK.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AuctionHouse.API.Controllers
{
    public class OrderController : ApiController
    {
        IOrderService _orderService;
        IOrderStatusService _orderStatusService;
        NLog.Logger logger;
        public OrderController(IOrderService orderService, IOrderStatusService orderStatusService)
        {
            _orderService = orderService;
            _orderStatusService = orderStatusService;
            logger = NLog.LogManager.GetCurrentClassLogger();
        }

        [HttpGet]
        [Route("api/order/marketownerorders/{id:int}")]
        public List<OrderDTO> GetOrdersFromMarket(int id)
        {
            try
            {
                var ordes = _orderService.GetOrdersForMarket(id).ToList();

                List<OrderDTO> orderToReturn = ordes.Select(x => new OrderDTO {

                    Customer = x.Customer,
                    DeliveryAdress = x.DeliveryAdress,
                    Id = x.Id,
                    OrderLines = x.OrderLines,
                    OrderStatus = x.OrderStatus,
                    PaymentMethod = x.PaymentMethod

                }).ToList();

                return orderToReturn;
            }
            catch (Exception e)
            {
                logger.Fatal(e.Message);
            }
            return null;
        }

        [HttpGet]
        [Route("api/order/{id:int}/accept")]
        public IHttpActionResult AcceptOrder(int id)
        {
            try
            {
                var order = _orderService.GetOrderById(id);
                order.StatusId = OrderStatusHelper.StatusAccepted(_orderStatusService).Id;
                _orderService.AcceptOrder(order);
                return Ok();
            }
            catch (Exception e)
            {
                logger.Fatal(e.Message);
            }
            return BadRequest();
        }
    }
}