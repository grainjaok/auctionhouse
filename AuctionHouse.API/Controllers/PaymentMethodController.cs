﻿using AuctionHouse.Domen.Entities;
using AuctionHouse.Domen.Interfaces;
using AuctionHouse.RestSDK.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AuctionHouse.API.Controllers
{
    public class PaymentMethodController : ApiController
    {
        IPaymentMethodService _paymentMethodService;
        NLog.Logger logger;

        public PaymentMethodController(IPaymentMethodService paymentMethodService)
        {
            _paymentMethodService = paymentMethodService;
            logger = NLog.LogManager.GetCurrentClassLogger();
        }

        [HttpPost]
        [Route("api/paymentmethod/create")]
        public IHttpActionResult Create([FromBody] PaymentMethodDTO method)
        {
            try
            {
                if (method != null)
                {
                    PaymentMethod methodToCreate = new PaymentMethod
                    {
                      MethodType = method.MethodType
                    };
                    _paymentMethodService.CreatePaymentMethod(methodToCreate);
                    return Ok();
                }
            }
            catch (Exception e)
            {
                logger.Fatal(e.Message);
            }
            return BadRequest();
        }

        [HttpGet]
        [Route("api/paymentmethods")]
        public List<PaymentMethodDTO> GetAll()
        {
            try
            {

                List<PaymentMethod> methods = _paymentMethodService.GetAllPaymentMethods().ToList();
                List<PaymentMethodDTO> methodsToReturn = methods.Select(x => new PaymentMethodDTO
                {
                    MethodType = x.MethodType,
                    Id = x.Id
                }).ToList();
                return methodsToReturn;
            }
            catch (Exception e)
            {
                logger.Fatal(e.Message);
            }
            return null;
        }
    }
}
