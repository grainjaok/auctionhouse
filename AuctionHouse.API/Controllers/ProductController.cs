﻿using AuctionHouse.Domen.Entities;
using AuctionHouse.Domen.Interfaces;
using AuctionHouse.RestSDK.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace AuctionHouse.API.Controllers
{
    public class ProductController : ApiController
    {
        IProductsService _productsService;
        IBetHistoryService _betHistoryService;
        NLog.Logger logger;
        public ProductController(IProductsService productService, IBetHistoryService betHistoryService)
        {
            _productsService = productService;
            _betHistoryService = betHistoryService;
            logger = NLog.LogManager.GetCurrentClassLogger();
        }
        [HttpPost]
        [Route("api/product/create")]
        public IHttpActionResult Create([FromBody] ProductDTO product)
        {
            try
            {
                if (product != null)
                {
                    Product productToCreate = new Product
                    {
                        AuctionEndDate = product.AuctionEndDate,
                        BuyOut = product.BuyOut,
                        CategoryId = product.CategoryId,
                        ImageUrl = product.ImageUrl,
                        MarketId = product.MarketId,
                        Name = product.Name,
                        StorageId = product.StorageId,
                        MinBet = product.MinBet,
                        PaymentMethodId = product.PaymentMethodId,
                        Number = product.Number,
                        Description = product.Description
                    };
                    _productsService.CreateProduct(productToCreate);
                    return Ok();
                }
            }
            catch (Exception e)
            {
                logger.Fatal(e.Message);
            }
            return BadRequest();
        }


        [HttpPost]
        [Route("api/product/createmany")]
        public IHttpActionResult CreateMany([FromBody] List<ProductDTO> products)
        {
            try
            {
                if (products != null)
                {
                    List<Product> productsToCreate = products.Select(x => new Product {
                        AuctionEndDate = x.AuctionEndDate,
                        BuyOut = x.BuyOut,
                        CategoryId = x.CategoryId,
                        ImageUrl = x.ImageUrl,
                        MarketId = x.MarketId,
                        Name = x.Name,
                        StorageId = x.StorageId,
                        MinBet = x.MinBet,
                        PaymentMethodId = x.PaymentMethodId,
                        Number = x.Number,
                        Description = x.Description
                    }).ToList();
                       
                    _productsService.CreateMany(productsToCreate);
                    return Ok();
                }
            }
            catch (Exception e)
            {
                logger.Fatal(e.Message);
            }
            return BadRequest();
        }

        [HttpGet]
        [Route("api/products")]
        public List<ProductDTO> GetAllProducts()
        {
            try
            {
                List<BetHistory> betHistory = _betHistoryService.GetAllHistories().ToList();
                List<Product> products = _productsService.GetProducts().ToList();

                List<ProductDTO> productsToReturn = products.Select(x => new ProductDTO
                {
                    AuctionEndDate = x.AuctionEndDate,
                    BuyOut = x.BuyOut,
                    Id = x.Id,
                    ImageUrl = x.ImageUrl,
                    MinBet = x.MinBet,
                    Name = x.Name,
                    Category = x.Category,
                    Storage = x.Storage,
                    Market = x.Market,
                    PaymentMethod = x.PaymentMethod,
                    CurrentBet = betHistory.Where(y => y.ProductId.Equals(x.Id)).Count() > 0 ? betHistory.Where(y => y.ProductId.Equals(x.Id)).Max(y => y.Bet) : 0,
                    Number = x.Number,
                    Description = x.Description
                }).ToList();
                
                return productsToReturn;
            }
            catch (Exception e)
            {
                logger.Fatal(e.Message);
            }
            return null;
        }

        [HttpDelete]
        [Route("api/product/delete/{id:int}")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _productsService.Delete(id);
                return Ok();
            }
            catch (Exception e)
            {
                logger.Fatal(e.Message);
            }
            return BadRequest();
        }


        [HttpGet]
        [Route("api/product/{id:int}")]
        public ProductDTO GetById(int id)
        {
            try
            {
                Product product = _productsService.GetProduct(id);
                ProductDTO productToReturn = new ProductDTO
                {
                    AuctionEndDate = product.AuctionEndDate,
                    BuyOut = product.BuyOut,
                    Id = product.Id,
                    ImageUrl = product.ImageUrl,
                    MinBet = product.MinBet,
                    Name = product.Name,
                    Category = product.Category,
                    Storage = product.Storage,
                    Market = product.Market,
                    PaymentMethod = product.PaymentMethod,
                    CurrentBet = _betHistoryService.GetHistoryForProduct(product.Id).Count()>0 ? _betHistoryService.GetHistoryForProduct(product.Id).Max(x=>x.Bet):0,
                    Number = product.Number,
                    Description = product.Description
                };

                return productToReturn;
            }
            catch (Exception e)
            {
                logger.Fatal(e.Message);
            }
            return null;
        }

        [HttpPut]
        [Route("api/product/edit/{id:int}")]
        public IHttpActionResult Edit([FromBody] ProductDTO product)
        {
            try
            {
                if (product != null)
                {
                    Product productToUpdate = new Product
                    {
                        AuctionEndDate = product.AuctionEndDate,
                        BuyOut = product.BuyOut,
                        CategoryId = product.CategoryId,
                        ImageUrl = product.ImageUrl,
                        MarketId = product.MarketId,
                        Name = product.Name,
                        StorageId = product.StorageId,
                        MinBet = product.MinBet,
                        Id = product.Id,
                        PaymentMethodId =product.PaymentMethodId,
                        Number = product.Number,
                        Description = product.Description

                    };
                    _productsService.EditProduct(productToUpdate);
                    return Ok();
                }
            }
            catch (Exception e)
            {
                logger.Fatal(e.Message);
            }
            return BadRequest();
        }
    }
}
