﻿using AuctionHouse.BLL.Services;
using AuctionHouse.Domen.Entities;
using AuctionHouse.Domen.Interfaces;
using AuctionHouse.RestSDK.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AuctionHouse.API.Controllers
{
    public class MarketController : ApiController
    {
        IMarketService _marketService;
        IProductsService _productService;
        IBetHistoryService _betHistoryService;
        NLog.Logger logger;

        public MarketController(IMarketService marketService, IProductsService productService, IBetHistoryService betHistoryService)
        {
            _productService = productService;
            _marketService = marketService;
            _betHistoryService = betHistoryService;
            logger = NLog.LogManager.GetCurrentClassLogger();
        }
        [HttpPost]
        [Route("api/market/create")]
        public IHttpActionResult Create([FromBody] MarketDTO market)
        {
            try
            {
                if (market != null)
                {
                    Market marketToCreate = new Market
                    {
                        Name = market.Name,
                        OwnerId = market.Owner
                    };
                    _marketService.CreateMarket(marketToCreate);
                    return Ok();
                }
            }
            catch (Exception e)
            {
                logger.Fatal(e.Message);
            }
            return BadRequest();
        }


        [HttpGet]
        [Route("api/markets")]
        public List<MarketDTO> GetAllMarkets()
        {
            try
            {
                List<Market> markets = _marketService.GetAllMarkets().ToList();


                List<MarketDTO> marketsToReturn = markets.Select(x => new MarketDTO
                {
                    Id = x.Id,
                    Name = x.Name,
                    Owner = x.OwnerId
                }).ToList();
                return marketsToReturn;
            }
            catch (Exception e)
            {
                logger.Fatal(e.Message);
            }

            return null;
        }
        [HttpGet]
        [Route("api/market/{id:int}/products")]
        public List<ProductDTO> GetProducts(int id)
        {
            try
            {
                List<BetHistory> betHistory = _betHistoryService.GetAllHistories().ToList();
                List<Product> products = _productService.GetMarketProducts(id).ToList();
                List<ProductDTO> productsToReturn = products.Select(x => new ProductDTO {
                    AuctionEndDate = x.AuctionEndDate,
                    BuyOut = x.BuyOut,
                    Id = x.Id,
                    ImageUrl = x.ImageUrl,
                    MinBet = x.MinBet,
                    Name = x.Name,
                    Category = x.Category,
                    Storage = x.Storage,
                    Market = x.Market,
                    PaymentMethod = x.PaymentMethod,
                    CurrentBet = betHistory.Where(y=>y.ProductId.Equals(x.Id)).Count()>0? betHistory.Where(y => y.ProductId.Equals(x.Id)).Max(y => y.Bet):0,
                    Number = x.Number,
                    Description = x.Description
                }).ToList();
                return productsToReturn;
            }
            catch (Exception e)
            {
                logger.Fatal(e.Message);
            }
            return null;
        }

        [HttpDelete]
        [Route("api/market/delete/{id:int}")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _marketService.Delete(id);
                var productsToRemove = _productService.GetProducts().Where(x => x.MarketId.Equals(id)).ToList();
                foreach (var product in productsToRemove)
                {
                    _productService.Delete(product.Id);
                }
                return Ok();
            }
            catch (Exception e)
            {
                logger.Fatal(e.Message);
            }
            return BadRequest();
        }

    }
}
