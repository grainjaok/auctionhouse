﻿using AuctionHouse.API.OrderCreation;
using AuctionHouse.Domen.Entities;
using AuctionHouse.Domen.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AuctionHouse.API.Controllers
{
    public class OrderLineController : ApiController
    {
        IProductsService _productsService;
        IBetHistoryService _betHistoryService;
        IOrderService _orderService;
        IOrderStatusService _orderStatusService;
        NLog.Logger logger;

        public OrderLineController(IProductsService productService, IBetHistoryService betHistoryService, IOrderService orderSerivce, IOrderStatusService orderStatusService)
        {
            _productsService = productService;
            _betHistoryService = betHistoryService;
            _orderService = orderSerivce;
            _orderStatusService = orderStatusService;
            logger = NLog.LogManager.GetCurrentClassLogger();
        }

        [HttpGet]
        [Route("api/orderline/create/{productId:int}")]
        public IHttpActionResult Create(int productId)
        {
            try
            {
                var product = _productsService.GetProduct(productId);
                var winningBet = _betHistoryService.GetHistoryForProduct(product.Id).OrderByDescending(x => x.Bet).First();
                OrderCreate.CreateOrder(product, _orderService, _orderStatusService, winningBet, _productsService);
                return Ok();
            }
            catch (Exception e)
            {
                logger.Fatal(e.Message);
            }
            return BadRequest();
        }

    }
}
