﻿using AuctionHouse.Domen.Helpers;
using AuctionHouse.RestSDK.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AuctionHouse.API.Controllers
{
    public class CurrencyController : ApiController
    {

        NLog.Logger logger;

        public CurrencyController()
        {           
            logger = NLog.LogManager.GetCurrentClassLogger();
        }

        [HttpGet]
        [Route("api/currency")]
        public CurrencyDTO GetCurrency()
        {
            try
            {
                var currency =  CurrencyLayerHelper.GetCurrancy();
                CurrencyDTO currencyToReturn = new CurrencyDTO {
                    EUR = currency.quotes.USDEUR,
                    UAH = currency.quotes.USDUAH
                };
                return currencyToReturn;
            }
            catch (Exception e)
            {
                logger.Fatal(e.Message);
            }
            return null;
        }
    }
}
