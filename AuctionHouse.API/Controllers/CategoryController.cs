﻿using AuctionHouse.Domen.Entities;
using AuctionHouse.Domen.Interfaces;
using AuctionHouse.RestSDK.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AuctionHouse.API.Controllers
{
    public class CategoryController : ApiController
    {
        ICategoryService _categoryService;
        IProductsService _productService;
        IBetHistoryService _betHistoryService;
        NLog.Logger logger;

        public CategoryController(ICategoryService categoryService, IProductsService productService, IBetHistoryService betHistoryService)
        {
            _categoryService = categoryService;
            _productService = productService;
            _betHistoryService = betHistoryService;
            logger = NLog.LogManager.GetCurrentClassLogger();
        }

        [HttpPost]
        [Route("api/category/create")]
        public IHttpActionResult Create([FromBody] CategoryDTO category)
        {
            try
            {
                if (category != null)
                {
                    Category categoryToCreate = new Category
                    {
                      CategoryType = category.CategoryType
                    };
                    _categoryService.Create(categoryToCreate);
                    return Ok();
                }
            }
            catch (Exception e)
            {      
                logger.Fatal(e.Message);
            }
            return BadRequest();
        }

        [HttpGet]
        [Route("api/categories")]
        public List<CategoryDTO> GetAll()
        {
            try
            {

                List<Category> categories = _categoryService.GetAll().ToList();
                List<CategoryDTO> categoriesToReturn = categories.Select(x => new CategoryDTO
                {
                    CategoryType = x.CategoryType,
                    Id = x.Id,
                    ParentId = x.ParentCategory.HasValue? x.ParentCategory.Value : 0
                }).ToList();
                return categoriesToReturn;
            }
            catch (Exception e)
            {
                logger.Fatal(e.Message);
            }
            return null;
        }

        [HttpGet]
        [Route("api/category/{id:int}/products")]
        public List<ProductDTO> GetProducts(int id)
        {
            try
            {
                List<BetHistory> betHistory = _betHistoryService.GetAllHistories().ToList();
                List<Product> products = _categoryService.GetProducts(id).ToList();
                List<ProductDTO> productsToReturn = products.Select(x => new ProductDTO
                {
                    AuctionEndDate = x.AuctionEndDate,
                    BuyOut = x.BuyOut,
                    Id = x.Id,
                    ImageUrl = x.ImageUrl,
                    MinBet = x.MinBet,
                    Name = x.Name,
                    Category = x.Category,
                    Storage = x.Storage,
                    Market = x.Market,
                    PaymentMethod = x.PaymentMethod,
                    CurrentBet = betHistory.Where(y=>y.ProductId.Equals(x.Id)).Count()>0? betHistory.Where(y => y.ProductId.Equals(x.Id)).Max(y => y.Bet):0,
                    Number = x.Number,
                    Description = x.Description

                }).ToList();
                return productsToReturn;
            }
            catch (Exception e)
            {
                logger.Fatal(e.Message);
            }
            return null;
        }

    }
}
