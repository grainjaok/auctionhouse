﻿using AuctionHouse.Domen.Entities;
using AuctionHouse.Domen.Interfaces;
using AuctionHouse.RestSDK.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AuctionHouse.API.Controllers
{
    public class StorageController : ApiController
    {

        IStorageService _storageService;
        NLog.Logger logger;
        public StorageController(IStorageService storageService)
        {
            _storageService = storageService;
            logger = NLog.LogManager.GetCurrentClassLogger();
        }
        [HttpPost]
        [Route("api/storage/create")]
        public IHttpActionResult Create([FromBody] StorageDTO storage)
        {
            try
            {
                if (storage != null)
                {
                    Storage storageToCreate = new Storage
                    {
                       Address = storage.Anddress,
                       Id = storage.Id,
                       Name = storage.Name
                    };
                    _storageService.CreateStorage(storageToCreate);
                    return Ok();
                }
            }
            catch (Exception e)
            {
                logger.Fatal(e.Message);
            }
            return BadRequest();
        }

        [HttpGet]
        [Route("api/storagies")]
        public List<StorageDTO> GetAllStoragies()
        {
            try
            {
                List<Storage> storagies = _storageService.GetAllStorages().ToList();


                List<StorageDTO> storagiesToReturn = storagies.Select(x => new StorageDTO
                {
                    Id = x.Id,
                    Name = x.Name,
                    Anddress = x.Address
                }).ToList();
                return storagiesToReturn;
            }
            catch (Exception e)
            {
                logger.Fatal(e.Message);
            }

            return null;
        }

    }
}
