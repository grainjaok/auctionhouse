﻿using AuctionHouse.API.App_Start;
using AuctionHouse.API.OrderCreation;
using Quartz;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace AuctionHouse.API
{
    public class WebApiApplication : System.Web.HttpApplication
    {

        protected void Application_Start()
        {
            SimpleInjectorWebApiInitializer.Initialize();

            GlobalConfiguration.Configure(WebApiConfig.Register);



            var sch = SimpleInjectorWebApiInitializer.container.GetInstance<OrderScheduler>();
            sch.Start();
        }

        protected void Application_Error()
        {
            Exception lastException = Server.GetLastError();
            NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
            logger.Fatal(lastException);
        }
    }
}