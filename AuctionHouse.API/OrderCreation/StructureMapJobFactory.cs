﻿using Quartz;
using Quartz.Spi;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace AuctionHouse.API.OrderCreation
{
    public class StructureMapJobFactory : IJobFactory
    {

        private readonly Container _container;

        public StructureMapJobFactory(Container container)
        {
            this._container = container;
        }

        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            try
            {
                return (IJob)_container.GetInstance(bundle.JobDetail.JobType);
            }
            catch (Exception e)
            {

                throw;
            }
           
        }

        public void ReturnJob(IJob job)
        {

        }
    }
}