﻿using AuctionHouse.Domen.Entities;
using AuctionHouse.Domen.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuctionHouse.API.OrderCreation
{
    public static class OrderCreate
    {
        public static void CreateOrder(Product product, IOrderService orderSerivce, BetHistory winningBet, IProductsService productService)
        {

            Order order = new Order
            {
                CustomerId = winningBet.UserId,
                DeliveryAdress = winningBet.User.Address,
                StatusId = 1,
                PaymentMethod = product.PaymentMethod.MethodType,

                OrderLines = new OrderLine
                {
                    NumberOfProducts = 1,
                    ProductId = product.Id,
                    CurrentBet = winningBet.Bet
                }
            };
            product.Number--;
            productService.EditProduct(product);
            orderSerivce.CreateOrder(order);
        }
    }
}