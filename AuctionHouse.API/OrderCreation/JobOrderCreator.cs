﻿using AuctionHouse.Domen.Interfaces;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuctionHouse.API.OrderCreation
{
    public class JobOrderCreator : IJob
    {
        IOrderService _orderService;
        IProductsService _productService;
        IBetHistoryService _betHistoryService;

        public JobOrderCreator(IProductsService productService, IBetHistoryService betHistoryService, IOrderService orderService)
        {
            _orderService = orderService;
            _productService = productService;
            _betHistoryService = betHistoryService;
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var productsEnds = _productService.GetEndedProducts().ToList();

                if (productsEnds.Count > 0)
                {
                    foreach (var product in productsEnds)
                    {
                        var betsForProduct = _betHistoryService.GetHistoryForProduct(product.Id);
                        if (betsForProduct.Count() > 0)
                        {
                            var winningBet = betsForProduct.OrderByDescending(x => x.Bet).First();
                            OrderCreate.CreateOrder(product, _orderService, winningBet, _productService);
                        }
                        else
                        {
                            product.Number--;
                            _productService.EditProduct(product);
                        }
                    }
                }
            }
            catch (Exception e)
            {

                //  throw;
            }


        }



    }
}