namespace AuctionHouse.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NumberOfProducts : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "Number", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "Number");
        }
    }
}
