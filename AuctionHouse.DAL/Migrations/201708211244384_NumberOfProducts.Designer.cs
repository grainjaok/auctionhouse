// <auto-generated />
namespace AuctionHouse.DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class NumberOfProducts : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(NumberOfProducts));
        
        string IMigrationMetadata.Id
        {
            get { return "201708211244384_NumberOfProducts"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
