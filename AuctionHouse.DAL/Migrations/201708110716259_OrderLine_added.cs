namespace AuctionHouse.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderLine_added : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Orders", "Product_Id", "dbo.Products");
            DropIndex("dbo.Orders", new[] { "Product_Id" });
            CreateTable(
                "dbo.OrderLines",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NumberOfProducts = c.Int(nullable: false),
                        CurrentBet = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedByApplicationUserId = c.String(),
                        LastUpdateDate = c.DateTime(),
                        LastUpdateByApplicationUserId = c.String(),
                        Order_Id = c.Int(),
                        Product_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Orders", t => t.Order_Id)
                .ForeignKey("dbo.Products", t => t.Product_Id)
                .Index(t => t.Order_Id)
                .Index(t => t.Product_Id);
            
            DropColumn("dbo.Orders", "Product_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders", "Product_Id", c => c.Int());
            DropForeignKey("dbo.OrderLines", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.OrderLines", "Order_Id", "dbo.Orders");
            DropIndex("dbo.OrderLines", new[] { "Product_Id" });
            DropIndex("dbo.OrderLines", new[] { "Order_Id" });
            DropTable("dbo.OrderLines");
            CreateIndex("dbo.Orders", "Product_Id");
            AddForeignKey("dbo.Orders", "Product_Id", "dbo.Products", "Id");
        }
    }
}
