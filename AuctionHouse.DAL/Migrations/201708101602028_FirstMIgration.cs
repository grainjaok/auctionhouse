namespace AuctionHouse.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstMIgration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrderProducts", "Order_Id", "dbo.Orders");
            DropForeignKey("dbo.OrderProducts", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.Products", "Cart_Id", "dbo.Carts");
            DropIndex("dbo.Products", new[] { "Cart_Id" });
            DropIndex("dbo.OrderProducts", new[] { "Order_Id" });
            DropIndex("dbo.OrderProducts", new[] { "Product_Id" });
            RenameColumn(table: "dbo.Orders", name: "CustomerId", newName: "Customer_Id");
            RenameIndex(table: "dbo.Orders", name: "IX_CustomerId", newName: "IX_Customer_Id");
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoryType = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedByApplicationUserId = c.String(),
                        LastUpdateDate = c.DateTime(),
                        LastUpdateByApplicationUserId = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Products", "AuctionEndDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Products", "MinBet", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Products", "CurrentBet", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Products", "Category_Id", c => c.Int());
            AddColumn("dbo.Markets", "Owner_Id", c => c.String(maxLength: 128));
            AddColumn("dbo.Orders", "DeliveryAdress", c => c.String());
            AddColumn("dbo.Orders", "PaymentMethod", c => c.String());
            AddColumn("dbo.Orders", "Product_Id", c => c.Int());
            CreateIndex("dbo.Markets", "Owner_Id");
            CreateIndex("dbo.Orders", "Product_Id");
            CreateIndex("dbo.Products", "Category_Id");
            AddForeignKey("dbo.Markets", "Owner_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Products", "Category_Id", "dbo.Categories", "Id");
            AddForeignKey("dbo.Orders", "Product_Id", "dbo.Products", "Id");
            DropColumn("dbo.Products", "Price");
            DropColumn("dbo.Products", "NumberOfProducts");
            DropColumn("dbo.Products", "Category");
            DropColumn("dbo.Products", "Cart_Id");
            DropColumn("dbo.AspNetUsers", "Balance");
            DropColumn("dbo.AspNetUsers", "CartId");
            DropTable("dbo.Carts");
            DropTable("dbo.ProductOrders");
            DropTable("dbo.OrderProducts");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.OrderProducts",
                c => new
                    {
                        Order_Id = c.Int(nullable: false),
                        Product_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Order_Id, t.Product_Id });
            
            CreateTable(
                "dbo.ProductOrders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductId = c.Int(nullable: false),
                        OrderId = c.Int(nullable: false),
                        ProductNumber = c.Int(nullable: false),
                        DeliveryAdress = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedByApplicationUserId = c.String(),
                        LastUpdateDate = c.DateTime(),
                        LastUpdateByApplicationUserId = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Carts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedByApplicationUserId = c.String(),
                        LastUpdateDate = c.DateTime(),
                        LastUpdateByApplicationUserId = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.AspNetUsers", "CartId", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "Balance", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Products", "Cart_Id", c => c.Int());
            AddColumn("dbo.Products", "Category", c => c.Int(nullable: false));
            AddColumn("dbo.Products", "NumberOfProducts", c => c.Int(nullable: false));
            AddColumn("dbo.Products", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropForeignKey("dbo.Orders", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.Products", "Category_Id", "dbo.Categories");
            DropForeignKey("dbo.Markets", "Owner_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Products", new[] { "Category_Id" });
            DropIndex("dbo.Orders", new[] { "Product_Id" });
            DropIndex("dbo.Markets", new[] { "Owner_Id" });
            DropColumn("dbo.Orders", "Product_Id");
            DropColumn("dbo.Orders", "PaymentMethod");
            DropColumn("dbo.Orders", "DeliveryAdress");
            DropColumn("dbo.Markets", "Owner_Id");
            DropColumn("dbo.Products", "Category_Id");
            DropColumn("dbo.Products", "CurrentBet");
            DropColumn("dbo.Products", "MinBet");
            DropColumn("dbo.Products", "AuctionEndDate");
            DropTable("dbo.Categories");
            RenameIndex(table: "dbo.Orders", name: "IX_Customer_Id", newName: "IX_CustomerId");
            RenameColumn(table: "dbo.Orders", name: "Customer_Id", newName: "CustomerId");
            CreateIndex("dbo.OrderProducts", "Product_Id");
            CreateIndex("dbo.OrderProducts", "Order_Id");
            CreateIndex("dbo.Products", "Cart_Id");
            AddForeignKey("dbo.Products", "Cart_Id", "dbo.Carts", "Id");
            AddForeignKey("dbo.OrderProducts", "Product_Id", "dbo.Products", "Id", cascadeDelete: true);
            AddForeignKey("dbo.OrderProducts", "Order_Id", "dbo.Orders", "Id", cascadeDelete: true);
        }
    }
}
