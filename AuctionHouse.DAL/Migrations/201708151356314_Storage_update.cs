namespace AuctionHouse.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Storage_update : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Storages", "Address", c => c.String());
            AlterColumn("dbo.Storages", "Name", c => c.String());
            DropColumn("dbo.Storages", "Adress");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Storages", "Adress", c => c.String());
            AlterColumn("dbo.Storages", "Name", c => c.Int(nullable: false));
            DropColumn("dbo.Storages", "Address");
        }
    }
}
