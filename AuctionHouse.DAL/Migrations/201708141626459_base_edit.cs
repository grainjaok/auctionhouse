namespace AuctionHouse.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class base_edit : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Categories", "DateCreated");
            DropColumn("dbo.Categories", "CreatedByApplicationUserId");
            DropColumn("dbo.Categories", "LastUpdateDate");
            DropColumn("dbo.Categories", "LastUpdateByApplicationUserId");
            DropColumn("dbo.Markets", "DateCreated");
            DropColumn("dbo.Markets", "CreatedByApplicationUserId");
            DropColumn("dbo.Markets", "LastUpdateDate");
            DropColumn("dbo.Markets", "LastUpdateByApplicationUserId");
            DropColumn("dbo.OrderLines", "DateCreated");
            DropColumn("dbo.OrderLines", "CreatedByApplicationUserId");
            DropColumn("dbo.OrderLines", "LastUpdateDate");
            DropColumn("dbo.OrderLines", "LastUpdateByApplicationUserId");
            DropColumn("dbo.Orders", "DateCreated");
            DropColumn("dbo.Orders", "CreatedByApplicationUserId");
            DropColumn("dbo.Orders", "LastUpdateDate");
            DropColumn("dbo.Orders", "LastUpdateByApplicationUserId");
            DropColumn("dbo.Products", "DateCreated");
            DropColumn("dbo.Products", "CreatedByApplicationUserId");
            DropColumn("dbo.Products", "LastUpdateDate");
            DropColumn("dbo.Products", "LastUpdateByApplicationUserId");
            DropColumn("dbo.Storages", "DateCreated");
            DropColumn("dbo.Storages", "CreatedByApplicationUserId");
            DropColumn("dbo.Storages", "LastUpdateDate");
            DropColumn("dbo.Storages", "LastUpdateByApplicationUserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Storages", "LastUpdateByApplicationUserId", c => c.String());
            AddColumn("dbo.Storages", "LastUpdateDate", c => c.DateTime());
            AddColumn("dbo.Storages", "CreatedByApplicationUserId", c => c.String());
            AddColumn("dbo.Storages", "DateCreated", c => c.DateTime(nullable: false));
            AddColumn("dbo.Products", "LastUpdateByApplicationUserId", c => c.String());
            AddColumn("dbo.Products", "LastUpdateDate", c => c.DateTime());
            AddColumn("dbo.Products", "CreatedByApplicationUserId", c => c.String());
            AddColumn("dbo.Products", "DateCreated", c => c.DateTime(nullable: false));
            AddColumn("dbo.Orders", "LastUpdateByApplicationUserId", c => c.String());
            AddColumn("dbo.Orders", "LastUpdateDate", c => c.DateTime());
            AddColumn("dbo.Orders", "CreatedByApplicationUserId", c => c.String());
            AddColumn("dbo.Orders", "DateCreated", c => c.DateTime(nullable: false));
            AddColumn("dbo.OrderLines", "LastUpdateByApplicationUserId", c => c.String());
            AddColumn("dbo.OrderLines", "LastUpdateDate", c => c.DateTime());
            AddColumn("dbo.OrderLines", "CreatedByApplicationUserId", c => c.String());
            AddColumn("dbo.OrderLines", "DateCreated", c => c.DateTime(nullable: false));
            AddColumn("dbo.Markets", "LastUpdateByApplicationUserId", c => c.String());
            AddColumn("dbo.Markets", "LastUpdateDate", c => c.DateTime());
            AddColumn("dbo.Markets", "CreatedByApplicationUserId", c => c.String());
            AddColumn("dbo.Markets", "DateCreated", c => c.DateTime(nullable: false));
            AddColumn("dbo.Categories", "LastUpdateByApplicationUserId", c => c.String());
            AddColumn("dbo.Categories", "LastUpdateDate", c => c.DateTime());
            AddColumn("dbo.Categories", "CreatedByApplicationUserId", c => c.String());
            AddColumn("dbo.Categories", "DateCreated", c => c.DateTime(nullable: false));
        }
    }
}
