namespace AuctionHouse.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderLIneInOrder : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrderLines", "OrderId", "dbo.Orders");
            DropIndex("dbo.OrderLines", new[] { "OrderId" });
            AddColumn("dbo.Orders", "OrderLineId", c => c.Int(nullable: false));
            CreateIndex("dbo.Orders", "OrderLineId");
            AddForeignKey("dbo.Orders", "OrderLineId", "dbo.OrderLines", "Id", cascadeDelete: true);
            DropColumn("dbo.OrderLines", "OrderId");
            DropColumn("dbo.Orders", "IsApproved");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders", "IsApproved", c => c.Boolean(nullable: false));
            AddColumn("dbo.OrderLines", "OrderId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Orders", "OrderLineId", "dbo.OrderLines");
            DropIndex("dbo.Orders", new[] { "OrderLineId" });
            DropColumn("dbo.Orders", "OrderLineId");
            CreateIndex("dbo.OrderLines", "OrderId");
            AddForeignKey("dbo.OrderLines", "OrderId", "dbo.Orders", "Id", cascadeDelete: true);
        }
    }
}
