namespace AuctionHouse.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class forgeryKey_adeed : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrderLines", "Order_Id", "dbo.Orders");
            DropForeignKey("dbo.OrderLines", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.Products", "Category_Id", "dbo.Categories");
            DropForeignKey("dbo.Products", "Market_Id", "dbo.Markets");
            DropForeignKey("dbo.Products", "Storage_Id", "dbo.Storages");
            DropIndex("dbo.OrderLines", new[] { "Order_Id" });
            DropIndex("dbo.OrderLines", new[] { "Product_Id" });
            DropIndex("dbo.Products", new[] { "Category_Id" });
            DropIndex("dbo.Products", new[] { "Market_Id" });
            DropIndex("dbo.Products", new[] { "Storage_Id" });
            RenameColumn(table: "dbo.Markets", name: "Owner_Id", newName: "OwnerId");
            RenameColumn(table: "dbo.OrderLines", name: "Order_Id", newName: "OrderId");
            RenameColumn(table: "dbo.OrderLines", name: "Product_Id", newName: "ProductId");
            RenameColumn(table: "dbo.Orders", name: "Customer_Id", newName: "CustomerId");
            RenameColumn(table: "dbo.Products", name: "Category_Id", newName: "CategoryId");
            RenameColumn(table: "dbo.Products", name: "Market_Id", newName: "MarketId");
            RenameColumn(table: "dbo.Products", name: "Storage_Id", newName: "StorageId");
            RenameIndex(table: "dbo.Markets", name: "IX_Owner_Id", newName: "IX_OwnerId");
            RenameIndex(table: "dbo.Orders", name: "IX_Customer_Id", newName: "IX_CustomerId");
            AlterColumn("dbo.OrderLines", "OrderId", c => c.Int(nullable: false));
            AlterColumn("dbo.OrderLines", "ProductId", c => c.Int(nullable: false));
            AlterColumn("dbo.Products", "CategoryId", c => c.Int(nullable: false));
            AlterColumn("dbo.Products", "MarketId", c => c.Int(nullable: false));
            AlterColumn("dbo.Products", "StorageId", c => c.Int(nullable: false));
            CreateIndex("dbo.OrderLines", "ProductId");
            CreateIndex("dbo.OrderLines", "OrderId");
            CreateIndex("dbo.Products", "CategoryId");
            CreateIndex("dbo.Products", "MarketId");
            CreateIndex("dbo.Products", "StorageId");
            AddForeignKey("dbo.OrderLines", "OrderId", "dbo.Orders", "Id", cascadeDelete: true);
            AddForeignKey("dbo.OrderLines", "ProductId", "dbo.Products", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Products", "CategoryId", "dbo.Categories", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Products", "MarketId", "dbo.Markets", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Products", "StorageId", "dbo.Storages", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "StorageId", "dbo.Storages");
            DropForeignKey("dbo.Products", "MarketId", "dbo.Markets");
            DropForeignKey("dbo.Products", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.OrderLines", "ProductId", "dbo.Products");
            DropForeignKey("dbo.OrderLines", "OrderId", "dbo.Orders");
            DropIndex("dbo.Products", new[] { "StorageId" });
            DropIndex("dbo.Products", new[] { "MarketId" });
            DropIndex("dbo.Products", new[] { "CategoryId" });
            DropIndex("dbo.OrderLines", new[] { "OrderId" });
            DropIndex("dbo.OrderLines", new[] { "ProductId" });
            AlterColumn("dbo.Products", "StorageId", c => c.Int());
            AlterColumn("dbo.Products", "MarketId", c => c.Int());
            AlterColumn("dbo.Products", "CategoryId", c => c.Int());
            AlterColumn("dbo.OrderLines", "ProductId", c => c.Int());
            AlterColumn("dbo.OrderLines", "OrderId", c => c.Int());
            RenameIndex(table: "dbo.Orders", name: "IX_CustomerId", newName: "IX_Customer_Id");
            RenameIndex(table: "dbo.Markets", name: "IX_OwnerId", newName: "IX_Owner_Id");
            RenameColumn(table: "dbo.Products", name: "StorageId", newName: "Storage_Id");
            RenameColumn(table: "dbo.Products", name: "MarketId", newName: "Market_Id");
            RenameColumn(table: "dbo.Products", name: "CategoryId", newName: "Category_Id");
            RenameColumn(table: "dbo.Orders", name: "CustomerId", newName: "Customer_Id");
            RenameColumn(table: "dbo.OrderLines", name: "ProductId", newName: "Product_Id");
            RenameColumn(table: "dbo.OrderLines", name: "OrderId", newName: "Order_Id");
            RenameColumn(table: "dbo.Markets", name: "OwnerId", newName: "Owner_Id");
            CreateIndex("dbo.Products", "Storage_Id");
            CreateIndex("dbo.Products", "Market_Id");
            CreateIndex("dbo.Products", "Category_Id");
            CreateIndex("dbo.OrderLines", "Product_Id");
            CreateIndex("dbo.OrderLines", "Order_Id");
            AddForeignKey("dbo.Products", "Storage_Id", "dbo.Storages", "Id");
            AddForeignKey("dbo.Products", "Market_Id", "dbo.Markets", "Id");
            AddForeignKey("dbo.Products", "Category_Id", "dbo.Categories", "Id");
            AddForeignKey("dbo.OrderLines", "Product_Id", "dbo.Products", "Id");
            AddForeignKey("dbo.OrderLines", "Order_Id", "dbo.Orders", "Id");
        }
    }
}
