namespace AuctionHouse.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class parentCategoryId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Categories", "ParentCategory", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Categories", "ParentCategory");
        }
    }
}
