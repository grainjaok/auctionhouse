namespace AuctionHouse.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class paymentMethodFixes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "PaymentMethodId", c => c.Int());
            AddColumn("dbo.Orders", "StatusId", c => c.Int(nullable: false));
            CreateIndex("dbo.Products", "PaymentMethodId");
            CreateIndex("dbo.Orders", "StatusId");
            AddForeignKey("dbo.Products", "PaymentMethodId", "dbo.PaymentMethods", "Id");
            AddForeignKey("dbo.Orders", "StatusId", "dbo.OrderStatus", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "StatusId", "dbo.OrderStatus");
            DropForeignKey("dbo.Products", "PaymentMethodId", "dbo.PaymentMethods");
            DropIndex("dbo.Orders", new[] { "StatusId" });
            DropIndex("dbo.Products", new[] { "PaymentMethodId" });
            DropColumn("dbo.Orders", "StatusId");
            DropColumn("dbo.Products", "PaymentMethodId");
        }
    }
}
