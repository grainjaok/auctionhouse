namespace AuctionHouse.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class History_Status_Method_added : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BetHistories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductId = c.Int(nullable: false),
                        UserId = c.String(maxLength: 128),
                        Bet = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.ProductId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.OrderStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StatusType = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PaymentMethods",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MethodType = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.AspNetUsers", "Address", c => c.String());
            DropColumn("dbo.Products", "CurrentBet");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Products", "CurrentBet", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropForeignKey("dbo.BetHistories", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.BetHistories", "ProductId", "dbo.Products");
            DropIndex("dbo.BetHistories", new[] { "UserId" });
            DropIndex("dbo.BetHistories", new[] { "ProductId" });
            DropColumn("dbo.AspNetUsers", "Address");
            DropTable("dbo.PaymentMethods");
            DropTable("dbo.OrderStatus");
            DropTable("dbo.BetHistories");
        }
    }
}
