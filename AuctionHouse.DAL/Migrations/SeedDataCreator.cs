﻿using AuctionHouse.DAL.EF;
using AuctionHouse.Domen.Entities;
using AuctionHouse.Domen.Helpers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.DAL.Migrations
{
    class SeedDataCreator
    {
        private readonly AuctionContext context;


        public SeedDataCreator(AuctionContext context)
        {
            this.context = context;
        }

        public void Seed()
        {
            AddRoles();
            //AddStatuses();
            UpdateOrCreateUser("Admin", "admin@blog.com", "zxcvbn", "London", AuthenticationHelper.ROLE_Admin);
            UpdateOrCreateUser("User", "user@blog.com", "zxcvbn", "Kharkiv", AuthenticationHelper.ROLE_USER);

        }
        private void AddRoles()
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            // Check to see if Role Exists, if not create it
            foreach (string role in AuthenticationHelper.GetAllRoles())
            {
                if (!roleManager.RoleExists(role))
                {
                    if (!(roleManager.Create(new IdentityRole(role)).Succeeded))
                    {
                        throw new Exception("User role: " + role + " was not created!");
                    }
                }
            }
        }

        private void AddStatuses()
        {
            foreach (string status in OrderStatusHelper.GetAllStatuses())
            {
                OrderStatus statusToCreate = new OrderStatus {
                    StatusType = status
                };
                context.OrderStatuses.Add(statusToCreate);
                context.SaveChanges();
            }
        }

        private ApplicationUser UpdateOrCreateUser(string username, string email, string password, string address, string role)
        {
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var user = manager.FindByName(username);
            if (null == user)
            {
                user = new ApplicationUser()
                {
                    UserName = username,
                    Email = email,
                    Address = address

                };

                manager.Create(user, password);

                context.SaveChanges();

                manager.RemoveFromRoles(user.Id, AuthenticationHelper.GetAllRoles().ToArray());
                manager.AddToRole(user.Id, role);
            }


            return user;
        }



    }


}
