namespace AuctionHouse.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PaymentMethod_notnullable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Products", "PaymentMethodId", "dbo.PaymentMethods");
            DropIndex("dbo.Products", new[] { "PaymentMethodId" });
            AlterColumn("dbo.Products", "PaymentMethodId", c => c.Int(nullable: false));
            CreateIndex("dbo.Products", "PaymentMethodId");
            AddForeignKey("dbo.Products", "PaymentMethodId", "dbo.PaymentMethods", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "PaymentMethodId", "dbo.PaymentMethods");
            DropIndex("dbo.Products", new[] { "PaymentMethodId" });
            AlterColumn("dbo.Products", "PaymentMethodId", c => c.Int());
            CreateIndex("dbo.Products", "PaymentMethodId");
            AddForeignKey("dbo.Products", "PaymentMethodId", "dbo.PaymentMethods", "Id");
        }
    }
}
