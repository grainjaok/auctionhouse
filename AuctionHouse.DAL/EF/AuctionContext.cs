﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using AuctionHouse.Domen.Entities;

namespace AuctionHouse.DAL.EF
{
    public class AuctionContext: IdentityDbContext<ApplicationUser>
    {
        public AuctionContext(): base("AuctionHouse")
        {

        }

        //public AuctionContext(string conectionString) : base() { }

        #region Entities
        public DbSet<Market> Markets { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Storage> Storages { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<OrderLine> OrderLines { get; set; }
        public DbSet<BetHistory> BetHistories { get; set; }
        public DbSet<PaymentMethod> PaymentMethods { get; set; }
        public DbSet<OrderStatus> OrderStatuses { get; set; }
        #endregion

    }
}
