﻿using AuctionHouse.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.DAL.Entities
{
    public class Market: BaseEntity
    {      
        public string Name { get; set; }

        public virtual ApplicationUser Owner { get; set; }
    }
}
