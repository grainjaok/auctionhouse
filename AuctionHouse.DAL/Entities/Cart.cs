﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.DAL.Entities
{
    public class Cart : BaseEntity
    {      
        public ApplicationUser User { get; set; }

        public virtual ICollection<Product> Products { get; set; } = new List<Product>();
    }
}
