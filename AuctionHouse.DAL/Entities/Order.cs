﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.DAL.Entities
{
    public class Order : BaseEntity
    {
        public string DeliveryAdress { get; set; }

        public bool IsApproved { get; set; }

        public string PaymentMethod { get; set; }

        public virtual ApplicationUser Customer { get; set; }

        //public virtual ICollection<Product> Products { get; set; } = new List<Product>();
    }
}
