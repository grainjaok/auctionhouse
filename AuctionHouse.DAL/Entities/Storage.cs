﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.DAL.Entities
{
    public class Storage : BaseEntity
    {        
        public int Name { get; set; }
        public string Adress { get; set; }
    }
}
