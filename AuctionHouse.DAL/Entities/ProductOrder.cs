﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.DAL.Entities
{
    public class ProductOrder : BaseEntity
    {
        public int ProductId { get; set; }
        public int OrderId { get; set; }
        public int ProductNumber { get; set; }
        public string DeliveryAdress { get; set; }
    }
}
