﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.DAL.Entities
{
    public class OrderLine:BaseEntity
    {
        public virtual Product Product { get; set; }
        public virtual Order Order { get; set; }
        public int NumberOfProducts { get; set; }
        public decimal CurrentBet { get; set; }
    }
}
