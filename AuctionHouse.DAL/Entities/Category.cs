﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.DAL.Entities
{
    public class Category :BaseEntity
    {
        public string CategoryType { get; set; }
    }
}
