﻿using AuctionHouse.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.DAL.Entities
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedByApplicationUserId { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public string LastUpdateByApplicationUserId { get; set; }
    }
}
