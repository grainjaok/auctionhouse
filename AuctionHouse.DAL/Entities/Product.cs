﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.DAL.Entities
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }

        public string ImageUrl { get; set; }

        public DateTime AuctionEndDate { get; set; }

        public decimal MinBet { get; set; }

        public decimal CurrentBet { get; set; }

        public virtual Category Category { get; set; }
       
        public virtual Market Market { get; set; }

        public virtual Storage Storage { get; set; }

        //public virtual ICollection<Order> Orders { get; set; } = new List<Order>();
    }
}
