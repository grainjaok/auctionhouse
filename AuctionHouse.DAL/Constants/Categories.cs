﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.DAL.Constants
{
    public enum Categories
    {
        Art = 0,
        Sculpture = 1,
        Jewellery = 2,
        Other = 3
    }
}
