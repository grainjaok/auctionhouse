﻿using AuctionHouse.DAL.Entities;
using AuctionHouse.DAL.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ApplicationUserManager UserManager { get; }
        ApplicationRoleManager RoleManager { get; }
        IRepository<Market> Markets { get; }
        IRepository<Order> Orders { get; }
        IRepository<Product> Products { get; }
        IRepository<Storage> Storages { get; }
        IRepository<Category> Categories { get; }
        IRepository<OrderLine> OrderLines { get; }
        void Save();
        Task SaveAsync();
    }
}
