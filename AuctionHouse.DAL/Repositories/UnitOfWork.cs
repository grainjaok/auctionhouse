﻿using AuctionHouse.DAL.EF;
using AuctionHouse.Domen.Entities;
using AuctionHouse.Domen.Identity;
using AuctionHouse.Domen.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        #region Fields
        private AuctionContext _context;
        private ApplicationUserManager userManager;
        private ApplicationRoleManager roleManager;       
        private GenericRepository<Market> marketRepository;
        private GenericRepository<Order> orderRepository;
        private GenericRepository<Product> productRepository;
        private GenericRepository<Storage> storageRepository;
        private GenericRepository<Category> categoryRepository;
        private GenericRepository<OrderLine> orderLineRepository;
        private GenericRepository<OrderStatus> orderStatusRepository;
        private GenericRepository<BetHistory> betHistoryRepository;
        private GenericRepository<PaymentMethod> paymentMethodRepository;

        #endregion

        
        public UnitOfWork()
        {
            _context = new AuctionContext();
            userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(_context));
            //roleManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(_context));
        }
        #region Singlton
        public ApplicationUserManager UserManager
        {
            get { return userManager; }
        }

        public ApplicationRoleManager RoleManager
        {
            get { return roleManager; }
        }

        public IRepository<Market> Markets
        {
            get
            {
                if (marketRepository == null)
                    marketRepository = new GenericRepository<Market>(_context);
                return marketRepository;
            }
        }

        public IRepository<Order> Orders
        {
            get
            {
                if (orderRepository == null)
                    orderRepository = new GenericRepository<Order>(_context);
                return orderRepository;
            }
        }

        public IRepository<Product> Products
        {
            get
            {
                if (productRepository == null)
                    productRepository = new GenericRepository<Product>(_context);
                return productRepository;
            }
        }

        public IRepository<Storage> Storages
        {
            get
            {
                if (storageRepository == null)
                    storageRepository = new GenericRepository<Storage>(_context);
                return storageRepository;
            }
        }

        public IRepository<Category> Categories
        {
            get
            {
                if (categoryRepository == null)
                    categoryRepository = new GenericRepository<Category>(_context);
                return categoryRepository;

            }
        }

        public IRepository<OrderLine> OrderLines
        {
            get
            {
                if (orderLineRepository == null)
                    orderLineRepository = new GenericRepository<OrderLine>(_context);
                return orderLineRepository;

            }
        }

        public IRepository<OrderStatus> OrderStatuses
        {
            get
            {
                if (orderStatusRepository == null)
                    orderStatusRepository = new GenericRepository<OrderStatus>(_context);
                return orderStatusRepository;

            }
        }

        public IRepository<BetHistory> BetHistories
        {
            get
            {
                if (betHistoryRepository == null)
                    betHistoryRepository = new GenericRepository<BetHistory>(_context);
                return betHistoryRepository;

            }
        }

        public IRepository<PaymentMethod> PaymentMethods
        {
            get
            {
                if (paymentMethodRepository == null)
                    paymentMethodRepository = new GenericRepository<PaymentMethod>(_context);
                return paymentMethodRepository;

            }
        }

        #endregion


        public void Save()
        {
            _context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    }

}
