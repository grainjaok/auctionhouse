﻿using AuctionHouse.Domen.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AuctionHouse.Models
{
    public class MarketViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Field must be entered")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        public string Owner { get; set; }
    }
}