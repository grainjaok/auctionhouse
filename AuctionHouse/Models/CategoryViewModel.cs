﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuctionHouse.Models
{
    public class CategoryViewModel
    {
        public int Id { get; set; }
        public string CategoryType { get; set; }
        public int ParentId { get; set; }

        public List<CategoryViewModel> ChildCategory { get; set; } = new List<CategoryViewModel>();
    }
}