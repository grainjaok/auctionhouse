﻿using AuctionHouse.Domen.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AuctionHouse.Models
{
    public class ProductViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Field must be entered")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        public string ImageUrl { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Auction ends date")]
        public DateTime AuctionEndDate { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:N2}")]
        public decimal MinBet { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:N2}")]
        public decimal CurrentBet { get; set; }

        public int Number { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:N2}")]
        public decimal BuyOut { get; set; }

        public string Description { get; set; }

        [DataType(DataType.Date)]
        public HttpPostedFileBase File { get; set; }

        public List<CategoryViewModel> Categories { get; set; }

        public List<StorageViewModel> Storagies { get; set; }

        public List<PaymentMethodViewModel> PaymentMethods { get; set; }

        public int PaymentMethodId { get; set; }

        public virtual PaymentMethod PaymentMethod { get; set; }

        public int CategoryId { get; set; }

        public virtual Category Category { get; set; }

        public int MarketId { get; set; }

        public virtual Market Market { get; set; }

        public int StorageId { get; set; }

        public virtual Storage Storage { get; set; }

    }
}