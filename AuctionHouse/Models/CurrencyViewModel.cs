﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuctionHouse.Models
{
    public class CurrencyViewModel
    {
        public string ActiveCurrency { get; set; }
        public List<string> Currencies { get; set; }
    }
}