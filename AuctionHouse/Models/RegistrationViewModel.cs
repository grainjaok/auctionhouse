﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuctionHouse.Models
{
    public class RegistrationViewModel
    {
        public string UserName { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }
    }
}