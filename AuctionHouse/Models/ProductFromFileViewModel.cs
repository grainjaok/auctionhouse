﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuctionHouse.Models
{
    public class ProductFromFileViewModel
    {    

        public string Name { get; set; }

        public string ImageUrl { get; set; }

        public string AuctionEndDate { get; set; }

        public decimal MinBet { get; set; }

        public decimal BuyOut { get; set; }

        public string Description { get; set; }

        public int Number { get; set; }

        public int CategoryId { get; set; }

        public int MarketId { get; set; }

        public int StorageId { get; set; }

        public int PaymentMethodId { get; set; }
    }

    public class ProductFromFileMap : CsvClassMap<ProductFromFileViewModel>
    {
         
    }
}