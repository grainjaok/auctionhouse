﻿using AuctionHouse.Models;
using AuctionHouse.RestSDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuctionHouse.Helpers
{
    public class GetCategories
    {
        Configuration Api_Configuration;
        Client client;
        public GetCategories()
        {
            Api_Configuration = new Configuration(AppSettings.UrlServerForSDK());

            client = new Client(Api_Configuration);
        }

        public List<CategoryViewModel> GetAllCategories()
        {
            var caterories = client.Categories.GetAll();
            List<CategoryViewModel> categoriesToReturn = caterories.Data.Select(x => new CategoryViewModel {
                CategoryType = x.CategoryType,
                Id = x.Id
            }).ToList();
            return categoriesToReturn;
        }

        public List<CategoryViewModel> GetCategoriesForMenu()
        {
            var caterories = client.Categories.GetAll();
  
            List<CategoryViewModel> categoriesToReturn = caterories.Data.Where(x=>x.ParentId ==0).Select(x => new CategoryViewModel
            {
                CategoryType = x.CategoryType,
                Id = x.Id,
                ChildCategory = caterories.Data.Where(y => y.ParentId.Equals(x.Id)).Select(y => new CategoryViewModel
                {
                    CategoryType = y.CategoryType,
                    Id = y.Id,
                    ParentId = y.ParentId
                }).ToList()
            }).ToList();
            return categoriesToReturn;
        }


    }
}