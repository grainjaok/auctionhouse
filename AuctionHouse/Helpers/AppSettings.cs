﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuctionHouse.Helpers
{
    public static class AppSettings
    {
        public static string UrlServerForSDK()
        {
            string value = System.Configuration.ConfigurationManager.AppSettings["AppApiUrl"];
            return value;           
        }
        public static string ImageFolderPath()
        {
            string value = System.Configuration.ConfigurationManager.AppSettings["ImageFolderPath"];
            return value;
        }
        public static int PaginationPageSize()
        {
            int value = Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["PaginationPageSize"]);
            return value;
        }        
    }
}
