﻿using AuctionHouse.Models;
using AuctionHouse.RestSDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuctionHouse.Helpers
{
    public class GetStoragies
    {
        Configuration Api_Configuration;
        Client client;
        public GetStoragies()
        {
            Api_Configuration = new Configuration(AppSettings.UrlServerForSDK());

            client = new Client(Api_Configuration);
        }

        public List<StorageViewModel> GetAllStoragies()
        {
            var storagies = client.Storages.Get();
            List<StorageViewModel> storagiesToReturn = storagies.Data.Select(x => new StorageViewModel
            {
                Address = x.Anddress,
                Name = x.Name,
                Id = x.Id
            }).ToList();
            return storagiesToReturn;
        }
    }
}
