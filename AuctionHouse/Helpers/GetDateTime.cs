﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace AuctionHouse.Helpers
{
    public static class GetDateTime
    {
        public static  DateTime GetCurrentTime(DateTime date)
        {
            var dateToReturn = date.AddMinutes(DateTime.Now.Minute).AddHours(DateTime.Now.Hour).AddSeconds(DateTime.Now.Second);
            return dateToReturn;
        }

    }
}