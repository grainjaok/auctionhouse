﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Web;

namespace AuctionHouse.Helpers
{
    public static class ImageSaveHelper
    {
        public static string SaveImage(string imgUrl, string filePath)
        {
            using (WebClient client = new WebClient())
            {           
                String fileName = Guid.NewGuid().ToString() +".jpg";                       
                client.DownloadFile(imgUrl, @""+ filePath + fileName);
                return fileName;
            }

        }
    }
}