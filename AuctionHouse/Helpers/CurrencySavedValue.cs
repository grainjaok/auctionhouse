﻿using AuctionHouse.RestSDK;
using AuctionHouse.RestSDK.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuctionHouse.Helpers
{
    public static class CurrencySavedValue
    {
        public static CurrencyDTO Currency { get; set; }
        public static DateTime LastUpdate { get; set; }

        public static CurrencyDTO GetCurrentCurrency()
        {
            if (DateTime.Now > LastUpdate.AddMinutes(20) || LastUpdate == null)
            {
                LastUpdate = DateTime.Now;
                var currency = CurrencyLayerClient.GetApiRequest().quotes;
                Currency = new CurrencyDTO {
                    EUR = currency.USDEUR,
                    UAH = currency.USDUAH
                };
            }
            return Currency;
        }
    }
}