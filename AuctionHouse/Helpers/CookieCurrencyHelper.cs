﻿using AuctionHouse.Domen.Helpers;
using AuctionHouse.Models;
using AuctionHouse.RestSDK;
using AuctionHouse.RestSDK.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuctionHouse.Helpers
{
    public static class CookieCurrencyHelper
    {
        public static void CheckCoockie()
        {
            if (HttpContext.Current.Request.Cookies["currency"] == null)
            {
                HttpCookie myCookie = new HttpCookie("currency");
                myCookie.Value = "USD";
                HttpContext.Current.Response.Cookies.Add(myCookie);
            }
        }

        public static CurrencyViewModel GetCurrentCurrecny()
        {
            if (HttpContext.Current.Request.Cookies["currency"] == null || HttpContext.Current.Request.Cookies["currency"].Value == "USD")
            {
                CurrencyViewModel currency = new CurrencyViewModel
                {
                    ActiveCurrency = "USD",

                    Currencies = new List<string> { "EUR", "UAH" }
                };
                return currency;
            }

            if (HttpContext.Current.Request.Cookies["currency"].Value == "EUR")
            {
                CurrencyViewModel currency = new CurrencyViewModel
                {
                    ActiveCurrency = "EUR",

                    Currencies = new List<string> { "USD", "UAH" }
                };
                return currency;
            }

            if (HttpContext.Current.Request.Cookies["currency"].Value == "UAH")
            {
                CurrencyViewModel currency = new CurrencyViewModel
                {
                    ActiveCurrency = "UAH",

                    Currencies = new List<string> { "EUR", "USD" }
                };
                return currency;
            }
            return null;
        }

        public static void SetCoockieValue(string value)
        {
            HttpCookie myCookie = new HttpCookie("currency");
            myCookie.Value = value;
            HttpContext.Current.Response.Cookies.Add(myCookie);
        }

        public static decimal NumberToMultiply()
        {
            HttpCookie myCookie = HttpContext.Current.Request.Cookies["currency"];
            CurrencyDTO currency = CurrencySavedValue.GetCurrentCurrency();

            var coockieValue = myCookie.Value; 

            if (coockieValue == "EUR")
            {
                return currency.EUR;
            }
            if (coockieValue == "UAH")
            {
                return currency.UAH;
            }

            return 1;
        }


    }
}