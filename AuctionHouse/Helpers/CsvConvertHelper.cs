﻿using AuctionHouse.Models;
using AuctionHouse.RestSDK.DTO;
using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace AuctionHouse.Helpers
{
    public static class CsvConvertHelper
    {
        public static List<ProductFromFileViewModel> GetProductsFromCsv(HttpPostedFileBase upload)
        {
            using (StreamReader reader = new StreamReader(upload.InputStream))
            {
                var csv = new CsvReader(reader);
                csv.Configuration.IgnoreHeaderWhiteSpace = true;
                var records = csv.GetRecords<ProductFromFileViewModel>().ToList();
                return records;
            }
           
        }
    }
}