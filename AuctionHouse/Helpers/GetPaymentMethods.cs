﻿using AuctionHouse.Models;
using AuctionHouse.RestSDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuctionHouse.Helpers
{
    public class GetPaymentMethods
    {
        Configuration Api_Configuration;
        Client client;
        public GetPaymentMethods()
        {
            Api_Configuration = new Configuration(AppSettings.UrlServerForSDK());

            client = new Client(Api_Configuration);
        }

        public List<PaymentMethodViewModel> GetAllPaymentMethods()
        {
            var methods = client.PaymentMethods.GetAll();
            List<PaymentMethodViewModel> methodsToReturn = methods.Data.Select(x => new PaymentMethodViewModel
            {
                MethodType = x.MethodType,
                Id = x.Id
            }).ToList();
            return methodsToReturn;
        }
    }
}