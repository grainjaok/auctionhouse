[assembly: WebActivator.PostApplicationStartMethod(typeof(AuctionHouse.App_Start.SimpleInjectorInitializer), "Initialize")]

namespace AuctionHouse.App_Start
{
    using BLL.Services;
    using DAL.EF;
    using DAL.Repositories;
    using Domen.Entities;
    using Domen.Identity;
    using Domen.Interfaces;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Microsoft.Owin;
    using Models;
    using SimpleInjector;
    using SimpleInjector.Integration.Web;
    using SimpleInjector.Integration.Web.Mvc;
    using System.Reflection;
    using System.Web;
    using System.Web.Mvc;

    public static class SimpleInjectorInitializer
    {
        /// <summary>Initialize the container and register it as MVC3 Dependency Resolver.</summary>
        public static void Initialize()
        {
            var container = new Container();

            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            InitializeContainer(container);

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }

        private static void InitializeContainer(Container container)
        {
            //container.Register<IUnitOfWork>(() => new UnitOfWork("AuctionHouse"), Lifestyle.Scoped);
            container.Register<IUnitOfWork, UnitOfWork>(Lifestyle.Scoped);
            container.Register<IOrderService, OrderService>(Lifestyle.Scoped);
            container.Register<IStorageService, StorageService>(Lifestyle.Scoped);
            container.Register<IProductsService, ProductService>(Lifestyle.Scoped);
            container.Register<IUserService, UserService>(Lifestyle.Scoped);
            container.Register<IMarketService, MarketService>(Lifestyle.Scoped);
            container.Register<ICategoryService, CategoryService>(Lifestyle.Scoped);
            container.Register<IBetHistoryService, BetHistoryService>(Lifestyle.Scoped);
            container.Register<IPaymentMethodService, PaymentMethodService>(Lifestyle.Scoped);
            container.Register<IOrderStatusService, OrderStatusService>(Lifestyle.Scoped);


            container.Register<AuctionContext>(Lifestyle.Scoped);
            container.Register<IUserStore<ApplicationUser>>(() => 
            new UserStore<ApplicationUser>(container.GetInstance<AuctionContext>()), Lifestyle.Scoped);
            container.Register<ApplicationUserManager>(Lifestyle.Scoped);

        }
    }

}