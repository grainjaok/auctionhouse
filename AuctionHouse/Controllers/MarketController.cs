﻿using AuctionHouse.Domen.Entities;
using AuctionHouse.Helpers;
using AuctionHouse.Models;
using AuctionHouse.RestSDK;
using AuctionHouse.RestSDK.DTO;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AuctionHouse.Controllers
{
    public class MarketController : Controller
    {
        Configuration Api_Configuration;
        Client client;
        public MarketController()
        {
            Api_Configuration = new Configuration(AppSettings.UrlServerForSDK());

            client = new Client(Api_Configuration);
      
        }

        [HttpGet]
        public ActionResult Create()
        {
       
            return View();
        }

        [HttpPost]
        public ActionResult Create(MarketViewModel market)
        {
            try
            {                
                if (ModelState.IsValid)
                {
                    MarketDTO marketToCreate = new MarketDTO
                    {
                        Name = market.Name,
                        Owner = HttpContext.User.Identity.GetUserId()
                    };
                    var serializedMarket = SerializeHelper.Serialize(marketToCreate);
                    var request = client.Markets.Create(serializedMarket);
                    return RedirectToAction("ViewAll","Market");
                }
            }
            catch (Exception)
            {

               // throw;
            }
            return View();
        }

        [HttpGet]
        public ActionResult ViewAll()
        {
            try
            {
                var response = client.Markets.Get();

                List<MarketViewModel> markets = response.Data.Select(x => new MarketViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Owner =x.Owner
                }).ToList(); ;

                return View(markets);
            }
            catch (Exception)
            {

                //throw;
            }
            return View();
        }

        public ActionResult GetProducts(int id, int page=1)
        {

            int pageSize = AppSettings.PaginationPageSize();

            try
            {   
                var response = client.Markets.GetProducts(id);
                List<ProductViewModel> productsToReturn = response.Data.Select(x => new ProductViewModel {
                    AuctionEndDate = x.AuctionEndDate,
                    BuyOut = Math.Round(x.BuyOut * CookieCurrencyHelper.NumberToMultiply()),
                    CurrentBet = Math.Round((x.CurrentBet > x.MinBet ? x.CurrentBet : x.MinBet) * CookieCurrencyHelper.NumberToMultiply()),
                    Id = x.Id,
                    ImageUrl = x.ImageUrl,
                    Name = x.Name,
                    Category = x.Category,
                    Storage = x.Storage,
                    Market = x.Market,
                    PaymentMethod = x.PaymentMethod,
                    Number = x.Number,
                    Description = x.Description,
                    MinBet = Math.Round(x.MinBet * CookieCurrencyHelper.NumberToMultiply())

                }).ToList();

                IEnumerable<ProductViewModel> productsPerPages = productsToReturn.Skip((page - 1) * pageSize).Take(pageSize);
                PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = productsToReturn.Count };
                PaginationProductViewModel pagiantionModel = new PaginationProductViewModel {PageInfo= pageInfo, Products=productsPerPages };
                return View(pagiantionModel);

            }
            catch (Exception)
            {

                //throw;
            }
            return View();
        }

        public ActionResult ViewUser()
        {
            try
            {
                var response = client.Markets.Get().Data.Where(x=>x.Owner.Equals(HttpContext.User.Identity.GetUserId()));
                List<MarketViewModel> markets = response.Select(x => new MarketViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Owner = x.Owner
                }).ToList();

                return View(markets);
            }
            catch (Exception)
            {
                //throw;
            }
            return null;
        }

        public ActionResult Delete(int id)
        {
            try
            {
                var request = client.Markets.Delete(id);
                return RedirectToAction("ViewUser", "Market");
            }
            catch (Exception)
            {
               // throw;
            }
            return null;
        }
    }
}