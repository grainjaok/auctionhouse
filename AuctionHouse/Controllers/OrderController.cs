﻿using AuctionHouse.Helpers;
using AuctionHouse.Models;
using AuctionHouse.RestSDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AuctionHouse.Controllers
{
    public class OrderController : Controller
    {
        Configuration Api_Configuration;
        Client client;
        public OrderController()
        {
            Api_Configuration = new Configuration(AppSettings.UrlServerForSDK());

            client = new Client(Api_Configuration);

        }

        public ActionResult ViewMarketOrder(int marketId)
        {
            try
            {
                var respone = client.ClientOrders.GetMarketOwnerOrders(marketId).Data;
                List<OrderViewModel> orderToView = respone.Select(x => new OrderViewModel {
                    Customer = x.Customer,
                    DeliveryAdress = x.DeliveryAdress,
                    Id = x.Id,
                    OrderLines = x.OrderLines,
                    OrderStatus = x.OrderStatus,
                    PaymentMethod = x.PaymentMethod
                }).ToList();

                ViewBag.MarketId = marketId;

                return View(orderToView);
            }
            catch (Exception)
            {

                //throw;
            }
            return null;
        }

        public ActionResult Accept(int orderId, int marketId)
        {
            try
            {
                var respone = client.ClientOrders.AcceptOrder(orderId);
                return RedirectToAction("ViewMarketOrder","Order", new { marketId = marketId});
            }
            catch (Exception)
            {

                //throw;
            }
            return null;
        }
    }
}