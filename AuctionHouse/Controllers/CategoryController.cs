﻿using AuctionHouse.Helpers;
using AuctionHouse.Models;
using AuctionHouse.RestSDK;
using AuctionHouse.RestSDK.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AuctionHouse.Controllers
{
    public class CategoryController : Controller
    {
        Configuration Api_Configuration;
        Client client;
        public CategoryController()
        {
            Api_Configuration = new Configuration(AppSettings.UrlServerForSDK());

            client = new Client(Api_Configuration);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(CategoryViewModel category)
        {
            if (ModelState.IsValid)
            {
                CategoryDTO categoryToCreate = new CategoryDTO
                {
                    CategoryType = category.CategoryType
                };
                var serializedCategory = SerializeHelper.Serialize(categoryToCreate);
                var request = client.Categories.Create(serializedCategory);
            }
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult GetProducts(int id, int page = 1)
        {
            int pageSize = AppSettings.PaginationPageSize();
            try
            {
                var response = client.Categories.GetProducts(id).Data;
                List<ProductViewModel> productsToReturn = response.Select(x => new ProductViewModel
                {
                    AuctionEndDate = x.AuctionEndDate,
                    BuyOut = Math.Round(x.BuyOut * CookieCurrencyHelper.NumberToMultiply()),
                    CurrentBet = Math.Round((x.CurrentBet > x.MinBet ? x.CurrentBet : x.MinBet) * CookieCurrencyHelper.NumberToMultiply()),
                    Id = x.Id,
                    ImageUrl = x.ImageUrl,
                    Name = x.Name,
                    Category = x.Category,
                    Storage = x.Storage,
                    Market = x.Market,
                    PaymentMethod = x.PaymentMethod,
                    Number = x.Number,
                    Description = x.Description
                }).ToList();
                IEnumerable<ProductViewModel> productsPerPages = productsToReturn.Skip((page - 1) * pageSize).Take(pageSize);
                PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = productsToReturn.Count };
                PaginationProductViewModel pagiantionModel = new PaginationProductViewModel { PageInfo = pageInfo, Products = productsPerPages };
                return View(pagiantionModel);
            }
            catch (Exception)
            {

                //throw;
            }
            return null;
        }
    }
}