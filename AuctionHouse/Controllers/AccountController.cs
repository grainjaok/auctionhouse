﻿using AuctionHouse.App_Start;
using AuctionHouse.Domen.Entities;
using AuctionHouse.Domen.Helpers;
using AuctionHouse.Domen.Identity;
using AuctionHouse.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AuctionHouse.Controllers
{
    public class AccountController : Controller
    {
        #region ctor and prop
       
        private ApplicationUserManager _userManager;

        public AccountController(ApplicationUserManager userManager)
        {           
            _userManager = userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();            
        }
        #endregion

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View(new LoginViewModel());
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (_userManager != null)
                    {
                        var user = await _userManager.FindAsync(model.UserName, model.Password);
                        if (user != null)
                        {
                            await SignInAsync(user, true);
                            return RedirectToLocal(returnUrl);
                        }
                    }
                }
                catch (Exception)
                {
                    //throw;
                }
            }
            return null;
        }
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Registration()
        {

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Registration(RegistrationViewModel user)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var userToCreate = new ApplicationUser()
                    {
                        UserName = user.UserName,
                        Email = user.Email,
                        Address = user.Address

                    };

                    var result = _userManager.Create(userToCreate, user.Password);

                    if (result.Succeeded)
                    {
                        _userManager.AddToRole(userToCreate.Id, AuthenticationHelper.ROLE_USER);
                        return RedirectToAction("Index", "Home");
                    }
                }
            }
            catch (Exception)
            {
                //throw;
            }
            return View(user);
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        [AllowAnonymous]
        public ActionResult LogOut()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home", new { area = "" });
        }


        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await _userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}
