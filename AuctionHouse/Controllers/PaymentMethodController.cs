﻿using AuctionHouse.Helpers;
using AuctionHouse.Models;
using AuctionHouse.RestSDK;
using AuctionHouse.RestSDK.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AuctionHouse.Controllers
{
    public class PaymentMethodController : Controller
    {
        Configuration Api_Configuration;
        Client client;
        public PaymentMethodController()
        {
            Api_Configuration = new Configuration(AppSettings.UrlServerForSDK());

            client = new Client(Api_Configuration);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(PaymentMethodViewModel method)
        {
            if (ModelState.IsValid)
            {
                PaymentMethodDTO methodToCreate = new PaymentMethodDTO
                {
                    MethodType = method.MethodType
                };
                var serializedMethod = SerializeHelper.Serialize(methodToCreate);
                var request = client.PaymentMethods.Create(serializedMethod);
            }
            return RedirectToAction("Index", "Home");
        }
    }
}