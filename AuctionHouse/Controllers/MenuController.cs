﻿using AuctionHouse.Helpers;
using AuctionHouse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AuctionHouse.Controllers
{
    public class MenuController : Controller
    {
        // GET: Menu
        public ActionResult MenuRenderHeader()
        {
            CookieCurrencyHelper.CheckCoockie();

            var currToReturn = CookieCurrencyHelper.GetCurrentCurrecny();

            return PartialView("_MenuRenderHeaderPartial", currToReturn);
        }
        public ActionResult MenuRenderSideBar()
        {
            List<CategoryViewModel> categoriesToView = new GetCategories().GetCategoriesForMenu();
            return PartialView("_MenuRenderSideBarPartial", categoriesToView);
        }

        public JsonResult SetCoockieValue(string value)
        {
            CookieCurrencyHelper.SetCoockieValue(value);
            return Json(true);
        }
    }
}