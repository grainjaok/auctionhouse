﻿using AuctionHouse.Domen.Helpers;
using AuctionHouse.Helpers;
using AuctionHouse.Models;
using AuctionHouse.RestSDK;
using AuctionHouse.RestSDK.DTO;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AuctionHouse.Controllers
{
    public class ProductController : Controller
    {
        Configuration Api_Configuration;
        Client client;
        public ProductController()
        {
            Api_Configuration = new Configuration(AppSettings.UrlServerForSDK());

            client = new Client(Api_Configuration);
        }
        [HttpGet]
        public ActionResult Create(int? marketId)
        {

            ProductViewModel product = new ProductViewModel
            {
                Storagies = new GetStoragies().GetAllStoragies(),
                Categories = new GetCategories().GetAllCategories(),
                PaymentMethods = new GetPaymentMethods().GetAllPaymentMethods()
            };
            ViewBag.MarketId = marketId.HasValue ? marketId.Value : 0;
            return View(product);

        }

        [HttpPost]
        public ActionResult Create(ProductViewModel product)
        {
            if (ModelState.IsValid)
            {
                string ImageName = String.Empty;
                if (product.File != null)
                {
                    ImageName = Guid.NewGuid().ToString() + ".jpg";
                    string physicalPath = AppSettings.ImageFolderPath() + ImageName;
                    product.File.SaveAs(physicalPath);
                }

                ProductDTO productToCreate = new ProductDTO
                {
                    AuctionEndDate = GetDateTime.GetCurrentTime(product.AuctionEndDate),
                    CategoryId = product.CategoryId,
                    ImageUrl = ImageName,
                    MarketId = product.MarketId,
                    MinBet = product.MinBet / CookieCurrencyHelper.NumberToMultiply(),
                    Name = product.Name,
                    StorageId = product.StorageId,
                    BuyOut = product.BuyOut / CookieCurrencyHelper.NumberToMultiply(),
                    PaymentMethodId = product.PaymentMethodId,
                    Number = product.Number == 0 ? 1 : product.Number,
                    Description = product.Description

                };
                var serializedProdcut = SerializeHelper.Serialize(productToCreate);
                var request = client.Products.Create(serializedProdcut);
                return RedirectToAction("ViewUser", "Market");
            }

            return View(product);
        }

        [HttpGet]
        public ActionResult GetProduct(int id)
        {
            try
            {
                var respone = client.Products.GetById(id).Data;

                ProductViewModel productToReturn = new ProductViewModel
                {
                    AuctionEndDate = respone.AuctionEndDate,
                    BuyOut = Math.Round(respone.BuyOut * CookieCurrencyHelper.NumberToMultiply()),
                    CurrentBet = Math.Round((respone.CurrentBet > respone.MinBet ? respone.CurrentBet : respone.MinBet) * CookieCurrencyHelper.NumberToMultiply()),
                    Id = respone.Id,
                    ImageUrl = respone.ImageUrl,
                    Name = respone.Name,
                    Category = respone.Category,
                    Storage = respone.Storage,
                    Market = respone.Market,
                    PaymentMethod = respone.PaymentMethod,
                    Number = respone.Number,
                    Description = respone.Description
                };

                return View(productToReturn);
            }
            catch (Exception)
            {

                //throw;
            }
            return null;
        }


        [HttpGet]
        public ActionResult GetAll()
        {
            try
            {


                var respone = client.Products.Get();
                List<ProductViewModel> productsToReturn = respone.Data.Select(x => new ProductViewModel
                {
                    AuctionEndDate = x.AuctionEndDate,
                    BuyOut = Math.Round(x.BuyOut * CookieCurrencyHelper.NumberToMultiply()),
                    CurrentBet = Math.Round((x.CurrentBet > x.MinBet ? x.CurrentBet : x.MinBet) * CookieCurrencyHelper.NumberToMultiply()),
                    Id = x.Id,
                    ImageUrl = x.ImageUrl,
                    Name = x.Name,
                    Category = x.Category,
                    Storage = x.Storage,
                    Market = x.Market,
                    PaymentMethod = x.PaymentMethod,
                    Number = x.Number,
                    Description = x.Description

                }).ToList();

                return PartialView(productsToReturn.OrderByDescending(x => x.Id).Take(6));
            }
            catch (Exception e)
            {

                //  throw;
            }
            return null;
        }

        [HttpGet]
        public ActionResult Delete(int marketId, int productId)
        {
            try
            {
                client.Products.Delete(productId);
                return RedirectToAction("GetProducts", "Market", new { id = marketId });
            }
            catch (Exception)
            {

                //  throw;
            }
            return null;
        }

        [HttpGet]
        public ActionResult Edit(int id, int marketId)
        {
            try
            {
                ViewBag.MarketId = marketId;

                var respone = client.Products.GetById(id).Data;
                ProductViewModel productToReturn = new ProductViewModel
                {
                    AuctionEndDate = respone.AuctionEndDate,
                    BuyOut = respone.BuyOut,
                    CurrentBet = respone.CurrentBet,
                    Id = respone.Id,
                    ImageUrl = respone.ImageUrl,
                    MinBet = respone.MinBet,
                    Name = respone.Name,
                    Category = respone.Category,
                    Storage = respone.Storage,
                    Market = respone.Market,
                    Number = respone.Number,
                    Description = respone.Description,
                    PaymentMethod = respone.PaymentMethod,
                    Storagies = new GetStoragies().GetAllStoragies(),
                    Categories = new GetCategories().GetAllCategories(),
                    PaymentMethods = new GetPaymentMethods().GetAllPaymentMethods()
                };
                return View(productToReturn);
            }
            catch (Exception)
            {

                // throw;
            }
            return null;
        }

        [HttpPost]
        public ActionResult Edit(ProductViewModel product)
        {
            try
            {
                string ImageName = "";
                if (product.File != null)
                {
                    ImageName = Guid.NewGuid().ToString() + ".jpg";
                    string physicalPath = AppSettings.ImageFolderPath() + ImageName;
                    product.File.SaveAs(physicalPath);
                }

                ProductDTO productToReturn = new ProductDTO
                {
                    AuctionEndDate = GetDateTime.GetCurrentTime(product.AuctionEndDate),
                    BuyOut = product.BuyOut,
                    CurrentBet = product.CurrentBet,
                    Id = product.Id,
                    ImageUrl = ImageName,
                    MinBet = product.MinBet,
                    Name = product.Name,
                    CategoryId = product.CategoryId,
                    StorageId = product.StorageId,
                    MarketId = product.MarketId,
                    PaymentMethodId = product.PaymentMethodId,
                    Number = product.Number,
                    Description = product.Description

                };
                var serializedProduct = SerializeHelper.Serialize(productToReturn);
                var respone = client.Products.Update(product.Id, serializedProduct);

                return RedirectToAction("ViewUser", "Market");
            }
            catch (Exception e)
            {

                // throw;
            }
            return null;
        }

        [HttpPost]
        public ActionResult CreateFromFile(HttpPostedFileBase upload)
        {
            try
            {
                if (upload != null && upload.ContentLength > 0)
                {

                    if (upload.FileName.EndsWith(".csv"))
                    {
                        var products = CsvConvertHelper.GetProductsFromCsv(upload);
                        List<ProductDTO> productsToCreate = products.Select(x => new ProductDTO
                        {
                            AuctionEndDate = DateTime.Parse(x.AuctionEndDate),
                            BuyOut = x.BuyOut,
                            CategoryId = x.CategoryId,
                            Description = x.Description,
                            MarketId = x.MarketId,
                            MinBet = x.MinBet,
                            Number = x.Number,
                            PaymentMethodId = x.PaymentMethodId,
                            StorageId = x.StorageId,
                            ImageUrl = ImageSaveHelper.SaveImage(x.ImageUrl, AppSettings.ImageFolderPath()),
                            Name = x.Name
                        }).ToList();
                        var serializedProducts = SerializeHelper.Serialize(productsToCreate);
                        var respone = client.Products.CreateMany(serializedProducts);
                        return RedirectToAction("ViewUser", "Market");
                    }
                    else
                    {
                        ModelState.AddModelError("File", "This file format is not supported");
                        return View();
                    }
                }
                else
                {
                    ModelState.AddModelError("File", "Please Upload Your file");
                }
            }
            catch (Exception e)
            {

                //throw;
            }
            return null;
        }


    }
}