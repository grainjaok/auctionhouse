﻿using AuctionHouse.Domen.Entities;
using AuctionHouse.Helpers;
using AuctionHouse.RestSDK;
using AuctionHouse.RestSDK.DTO;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AuctionHouse.Controllers
{
    public class BetHistoryController : Controller
    {
        Configuration Api_Configuration;
        Client client;
        public BetHistoryController()
        {
            Api_Configuration = new Configuration(AppSettings.UrlServerForSDK());

            client = new Client(Api_Configuration);

        }

        [HttpPost]
        public JsonResult Create(int id, decimal bet)
        {
            try
            {
                bet = bet / CookieCurrencyHelper.NumberToMultiply();
                BetHistoryDTO historyToCreate = new BetHistoryDTO {
                    Bet = bet,
                    ProductId = id,
                    UserId = HttpContext.User.Identity.GetUserId()
                };                
                var serializedHistory = SerializeHelper.Serialize(historyToCreate);
                var request = client.ClientBetHistories.Create(serializedHistory);

                var productRespone = client.Products.GetById(id).Data;
                if (bet>=productRespone.BuyOut)
                {
                    client.ClientOrderLInes.Create(id);
                }

                return Json(true);
            }
            catch (Exception)
            {

                //throw;
            }
            return null;
        }
    }
}