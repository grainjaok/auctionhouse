﻿using AuctionHouse.Helpers;
using AuctionHouse.Models;
using AuctionHouse.RestSDK;
using AuctionHouse.RestSDK.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AuctionHouse.Controllers
{
    public class StorageController : Controller
    {
        Configuration Api_Configuration;
        Client client;
        public StorageController()
        {
            Api_Configuration = new Configuration(AppSettings.UrlServerForSDK());

            client = new Client(Api_Configuration);

        }

        [HttpGet]
        public ActionResult Create()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Create(StorageViewModel storage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    StorageDTO storageToCreate = new StorageDTO {
                        Anddress = storage.Address,
                        Name = storage.Name
                    };
                    var serializedStorage = SerializeHelper.Serialize(storageToCreate);
                    var request = client.Storages.Create(serializedStorage);
                    return RedirectToAction("ViewUser", "Market");
                }

                return RedirectToAction("ViewUser", "Market");
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}