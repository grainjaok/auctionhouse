﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.Domen.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        T GetById(int id);
        IEnumerable<T> Get(Func<T, bool> predicate);
        IEnumerable<T> Find(Func<T, Boolean> predicate);
        void Create(T item);
        void CreateMany(List<T> list);
        void Update(T item);
        void Delete(int id);
    }
}
