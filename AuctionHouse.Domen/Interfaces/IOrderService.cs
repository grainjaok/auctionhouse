﻿using AuctionHouse.Domen.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.Domen.Interfaces
{
    public interface IOrderService
    {
        void CreateOrder(Order order);
        IEnumerable<Order> GetOrdersForMarket(int id);
        void AcceptOrder(Order order);
        Order GetOrderById(int id);
        void Dispose();
    }
}

