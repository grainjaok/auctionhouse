﻿using AuctionHouse.Domen.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.Domen.Interfaces
{
    public interface IMarketService
    {
        void CreateMarket(Market market);
        IEnumerable<Market> GetAllMarkets();
        Market GetMarketById(int? id);
        void Delete(int id);
        void Dispose();
    }
}
