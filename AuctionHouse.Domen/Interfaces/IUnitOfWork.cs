﻿using AuctionHouse.Domen.Entities;
using AuctionHouse.Domen.Identity;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.Domen.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ApplicationUserManager UserManager { get; }
        ApplicationRoleManager RoleManager { get; }
        IRepository<Market> Markets { get; }
        IRepository<Order> Orders { get; }
        IRepository<Product> Products { get; }
        IRepository<Storage> Storages { get; }
        IRepository<Category> Categories { get; }
        IRepository<OrderLine> OrderLines { get; }
        IRepository<BetHistory> BetHistories { get; }
        IRepository<OrderStatus> OrderStatuses { get; }
        IRepository<PaymentMethod> PaymentMethods { get; }
        void Save();
        Task SaveAsync();
    }
}
