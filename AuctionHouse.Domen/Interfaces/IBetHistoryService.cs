﻿using AuctionHouse.Domen.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.Domen.Interfaces
{
    public interface IBetHistoryService
    {
        void CreateBetHistory(BetHistory betHistory);
        IEnumerable<BetHistory> GetHistoryForProduct(int id);
        IEnumerable<BetHistory> GetAllHistories();
        void Dispose();
    }
}
