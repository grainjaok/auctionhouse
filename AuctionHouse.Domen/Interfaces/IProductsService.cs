﻿using AuctionHouse.Domen.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.Domen.Interfaces
{
    public interface IProductsService
    {
        void CreateProduct(Product product);
        Product GetProduct(int id);
        IEnumerable<Product> GetProducts();
        IEnumerable<Product> GetMarketProducts(int id);
        IEnumerable<Product> GetEndedProducts();
        void CreateMany(List<Product>list);
        void EditProduct(Product product);
        void Delete(int id);
        void Dispose();
    }
}
