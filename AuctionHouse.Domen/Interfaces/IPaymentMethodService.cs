﻿using AuctionHouse.Domen.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.Domen.Interfaces
{
    public interface IPaymentMethodService
    {
        void CreatePaymentMethod(PaymentMethod paymentMethod);
        IEnumerable<PaymentMethod> GetAllPaymentMethods();
        void Dispose();
    }
}
