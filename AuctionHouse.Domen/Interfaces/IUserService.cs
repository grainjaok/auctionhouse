﻿using AuctionHouse.Domen.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.Domen.Interfaces
{
   public interface IUserService
    {
        UserInfoHelper GeneralUserInfo();
        void Dispose();
    }
}
