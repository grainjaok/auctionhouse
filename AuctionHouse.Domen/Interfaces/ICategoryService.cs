﻿using AuctionHouse.Domen.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.Domen.Interfaces
{
    public interface ICategoryService
    {
        void Create(Category category);
        IEnumerable<Category> GetAll();
        IEnumerable<Product> GetProducts(int id);
        void Dispose();
    }
}
