﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.Domen.Helpers
{
    public static class CurrencyLayerHelper
    {
        private static CurrencyLayerModel currancyLayer;

        public static void SetNewCurrancy(CurrencyLayerModel newCurrency)
        {
            currancyLayer = new CurrencyLayerModel { quotes = newCurrency.quotes };

        }

        public static CurrencyLayerModel GetCurrancy()
        {
            return currancyLayer;
        }
    }
}
