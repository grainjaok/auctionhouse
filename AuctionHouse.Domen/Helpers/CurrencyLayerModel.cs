﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.Domen.Helpers
{
    public class CurrencyLayerModel
    {
        public class Quotes
        {
            public decimal USDUAH { get; set; }
            public decimal USDEUR { get; set; }
        }
     
            public bool success { get; set; }
            public string terms { get; set; }
            public string privacy { get; set; }
            public int timestamp { get; set; }
            public string source { get; set; }
            public Quotes quotes { get; set; }
       
    }


}
