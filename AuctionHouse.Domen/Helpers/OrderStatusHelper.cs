﻿using AuctionHouse.Domen.Entities;
using AuctionHouse.Domen.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.Domen.Helpers
{
    public class OrderStatusHelper
    {
        public const string STATUS_NEW = "New";
        public const string STATUS_ACCEPTED = "Accepted";
        //public const string STATUS_DECLAINED= "Declained";
        //public const string STATUS_FINIHSED = "Finished";

        public static List<String> GetAllStatuses()
        {
            List<String> statuses = new List<string>();
            statuses.Add(STATUS_NEW);
            statuses.Add(STATUS_ACCEPTED);
            //statuses.Add(STATUS_DECLAINED);
            //statuses.Add(STATUS_FINIHSED);
            return statuses;
        }

        public static OrderStatus StatusNew(IOrderStatusService service)
        {
            return service.GetOrderStatus(STATUS_NEW);
        }

        public static OrderStatus StatusAccepted(IOrderStatusService service)
        {
            return service.GetOrderStatus(STATUS_ACCEPTED);
        }
    }
}
