﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.Domen.Entities
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }

        public string ImageUrl { get; set; }

        public DateTime AuctionEndDate { get; set; }

        public decimal MinBet { get; set; }

        public decimal BuyOut { get; set; }

        public int CategoryId { get; set; }

        public int Number { get; set; }

        public string Description { get; set; }

        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }

        public int MarketId { get; set; }

        [ForeignKey("MarketId")]
        public virtual Market Market { get; set; }

        public int StorageId { get; set; }

        [ForeignKey("StorageId")]
        public virtual Storage Storage { get; set; }

        public int PaymentMethodId { get; set; }

        [ForeignKey("PaymentMethodId")]
        public virtual PaymentMethod PaymentMethod { get; set; }


    }
}
