﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.Domen.Entities
{
    public class Order : BaseEntity
    {
        public string DeliveryAdress { get; set; }

        public string PaymentMethod { get; set; }

        public string CustomerId { get; set; }

        [ForeignKey("CustomerId")]
        public virtual ApplicationUser Customer { get; set; }

        public int StatusId { get; set; }

        [ForeignKey("StatusId")]
        public virtual OrderStatus OrderStatus { get; set; }

        public int OrderLineId { get; set; }

        [ForeignKey("OrderLineId")]
        public virtual OrderLine OrderLines { get; set; }
    }
}
