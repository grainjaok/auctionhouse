﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.Domen.Entities
{
    public class PaymentMethod :BaseEntity
    {
        public string MethodType { get; set; }
    }
}
