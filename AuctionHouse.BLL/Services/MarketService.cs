﻿using AuctionHouse.Domen.Entities;
using AuctionHouse.Domen.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.BLL.Services
{
   public class MarketService : IMarketService
    {
        IUnitOfWork _unitOfWork { get; set; }

        public MarketService(IUnitOfWork uow)
        {
            _unitOfWork = uow;
        }

        public void CreateMarket(Market market)
        {
            _unitOfWork.Markets.Create(market);         
        }

        public IEnumerable<Market> GetAllMarkets()
        {
            return _unitOfWork.Markets.GetAll();
        }

        public Market GetMarketById(int? id)
        {
            return _unitOfWork.Markets.GetById(id.Value);
        }

        public void Delete(int id)
        {
            _unitOfWork.Markets.Delete(id);
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }
    }
}
