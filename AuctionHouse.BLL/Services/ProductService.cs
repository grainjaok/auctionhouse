﻿using AuctionHouse.Domen.Entities;
using AuctionHouse.Domen.Interfaces;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.BLL.Services
{
    public class ProductService : IProductsService
    {
        IUnitOfWork _unitOfWork { get; set; }

        public ProductService(IUnitOfWork uow)
        {
            _unitOfWork = uow;
        }

        public void CreateProduct(Product product)
        {
            _unitOfWork.Products.Create(product);
        }

        public void CreateMany(List<Product> list)
        {
            _unitOfWork.Products.CreateMany(list);
        }

        public IEnumerable<Product> GetProducts()
        {
            return _unitOfWork.Products.GetAll().Where(x=> !x.Number.Equals(0));
        }

        public IEnumerable<Product> GetMarketProducts(int id)
        {
            return _unitOfWork.Products.Get(x => x.MarketId.Equals(id)).Where(x => !x.Number.Equals(0)); 
        }

        public void EditProduct(Product product)
        {
            _unitOfWork.Products.Update(product);
        }

        public Product GetProduct(int id)
        {
            return _unitOfWork.Products.GetById(id);
        }

        public IEnumerable<Product> GetEndedProducts()
        {
            IEnumerable<Product> products = Enumerable.Empty<Product>();

            products = from product in _unitOfWork.Products.Get(x=>x.Number>0 && x.AuctionEndDate<DateTime.Now)
                      join orderLine in _unitOfWork.OrderLines.GetAll()
                      on product.Id equals orderLine.ProductId into joinData
                      from left in joinData.DefaultIfEmpty()                 
                      select new Product {
                          AuctionEndDate = product.AuctionEndDate,
                          BuyOut = product.BuyOut,
                          Category = product.Category,
                          Id = product.Id,
                          ImageUrl = product.ImageUrl,
                          Market = product.Market,
                          MinBet = product.MinBet,
                          Name = product.Name,
                          PaymentMethod = product.PaymentMethod,
                          Storage = product.Storage,
                          Number = product.Number,
                          CategoryId = product.CategoryId,
                          MarketId = product.MarketId,
                          PaymentMethodId = product.PaymentMethodId,
                          StorageId = product.StorageId                          
                      };       
            return products;
        }

        public void Delete(int id)
        {
            _unitOfWork.Products.Delete(id);
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }
    }
}
