﻿using AuctionHouse.Domen.Entities;
using AuctionHouse.Domen.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.BLL.Services
{
    public class OrderStatusService : IOrderStatusService
    {
        IUnitOfWork _unitOfWork { get; set; }

        public OrderStatusService(IUnitOfWork uow)
        {
            _unitOfWork = uow;
        }

        public OrderStatus GetOrderStatus(string name)
        {
            return _unitOfWork.OrderStatuses.Get(x => x.StatusType.Equals(name)).FirstOrDefault();
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }
    }
}
