﻿using AuctionHouse.Domen.Entities;
using AuctionHouse.Domen.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.BLL.Services
{
   public class CategoryService : ICategoryService
    {
        IUnitOfWork _unitOfWork { get; set; }

        public CategoryService(IUnitOfWork uow)
        {
            _unitOfWork = uow;
        }

        public void Create(Category category)
        {
            _unitOfWork.Categories.Create(category);
        }

        public IEnumerable<Category> GetAll()
        {
            return _unitOfWork.Categories.GetAll();
        }

        public IEnumerable<Product> GetProducts(int id)
        {
            return _unitOfWork.Products.Get(x => x.CategoryId.Equals(id)).Where(x => !x.Number.Equals(0));
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }
    }
}
