﻿using AuctionHouse.Domen.Entities;
using AuctionHouse.Domen.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.BLL.Services
{
    public class OrderLineService : IOrderLineService
    {
        IUnitOfWork _unitOfWork { get; set; }

        public OrderLineService(IUnitOfWork uow)
        {
            _unitOfWork = uow;
        }

        public void CreateOrderLine(OrderLine orderLine)
        {
            _unitOfWork.OrderLines.Create(orderLine);
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }
    }
}
