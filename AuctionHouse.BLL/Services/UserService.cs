﻿using AuctionHouse.Domen.Helpers;
using AuctionHouse.Domen.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.BLL.Services
{
    public class UserService : IUserService
    {
        IUnitOfWork _unitOfWork { get; set; }

        public UserService(IUnitOfWork uow)
        {
            _unitOfWork = uow;
        }

        public UserInfoHelper GeneralUserInfo()
        {

            return null;
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }
    }
}
