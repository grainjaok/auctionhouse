﻿using AuctionHouse.Domen.Entities;
using AuctionHouse.Domen.Helpers;
using AuctionHouse.Domen.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.BLL.Services
{
    public class OrderService : IOrderService
    {
        IUnitOfWork _unitOfWork { get; set; }
        IOrderStatusService _orderStattusService;
        
        public OrderService(IUnitOfWork uow, IOrderStatusService orderStatusSevice)
        {
            _orderStattusService = orderStatusSevice;
            _unitOfWork = uow;
        }

        public void CreateOrder(Order order)
        {
            _unitOfWork.Orders.Create(order);
         
        }

        public IEnumerable<Order> GetOrdersForMarket(int id)
        {
            var orderStatusNew = OrderStatusHelper.StatusNew(_orderStattusService).Id;

            var orders = from product in _unitOfWork.Products.Get(p => p.MarketId.Equals(id))
                         join orderLine in _unitOfWork.OrderLines.GetAll()
                         on product.Id equals orderLine.ProductId
                         join order in _unitOfWork.Orders.Get(o => o.StatusId == orderStatusNew)
                         on orderLine.Id equals order.OrderLineId
                         select new Order
                         {
                             Customer = order.Customer,
                             DeliveryAdress = order.DeliveryAdress,
                             OrderLines = order.OrderLines,
                             OrderStatus = order.OrderStatus,
                             PaymentMethod = order.PaymentMethod,                             
                             Id = order.Id
                         };

            return orders;
        }

        public void AcceptOrder(Order order)
        {

            _unitOfWork.Orders.Update(order);
        }

        public Order GetOrderById(int id)
        {
            return _unitOfWork.Orders.GetById(id);
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }
    }
}
