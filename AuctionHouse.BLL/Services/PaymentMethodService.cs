﻿using AuctionHouse.Domen.Entities;
using AuctionHouse.Domen.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.BLL.Services
{
    public class PaymentMethodService: IPaymentMethodService
    {
        IUnitOfWork _unitOfWork { get; set; }

        public PaymentMethodService(IUnitOfWork uow)
        {
            _unitOfWork = uow;
        }

        public void CreatePaymentMethod(PaymentMethod method)
        {
            _unitOfWork.PaymentMethods.Create(method);
        }

        public IEnumerable<PaymentMethod> GetAllPaymentMethods()
        {
            return _unitOfWork.PaymentMethods.GetAll();
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }
    }
}
