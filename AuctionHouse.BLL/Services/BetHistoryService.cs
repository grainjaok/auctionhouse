﻿using AuctionHouse.Domen.Entities;
using AuctionHouse.Domen.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.BLL.Services
{
    public class BetHistoryService : IBetHistoryService
    {
        IUnitOfWork _unitOfWork { get; set; }

        public BetHistoryService(IUnitOfWork uow)
        {
            _unitOfWork = uow;
        }

        public void CreateBetHistory(BetHistory betHistory)
        {
            _unitOfWork.BetHistories.Create(betHistory);
        }
        public IEnumerable<BetHistory> GetHistoryForProduct(int id)
        {
            return _unitOfWork.BetHistories.Get(x => x.ProductId.Equals(id));
        }

        public IEnumerable<BetHistory> GetAllHistories()
        {
            return _unitOfWork.BetHistories.GetAll();
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }
    }
}
