﻿using AuctionHouse.Domen.Entities;
using AuctionHouse.Domen.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.BLL.Services
{
    public class StorageService : IStorageService
    {
        IUnitOfWork _unitOfWork { get; set; }

        public StorageService(IUnitOfWork uow)
        {
            _unitOfWork = uow;
        }

        public void CreateStorage(Storage storage)
        {

            _unitOfWork.Storages.Create(storage);
        }

        public IEnumerable<Storage> GetAllStorages()
        {
            return _unitOfWork.Storages.GetAll();
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }
    }
}
