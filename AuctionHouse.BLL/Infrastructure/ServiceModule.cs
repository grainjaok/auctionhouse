﻿using AuctionHouse.DAL.Interfaces;
using AuctionHouse.DAL.Repositories;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AuctionHouse.BLL.Infrastructure
{

    public class ServiceModule
    {
        readonly Container container;
        private string connectionString;
        public ServiceModule(string connection)
        {
            container = new Container();
            connectionString = connection;
        }

        public void BusinessInject()
        {
            container.Register<IUnitOfWork>(() => new UnitOfWork(connectionString));
            container.Verify();
        }
        public void PersistanceInject()
        {

        }
        
       
        
       
    }
}
