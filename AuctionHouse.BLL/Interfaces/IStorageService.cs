﻿using AuctionHouse.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.BLL.Interfaces
{
    public interface IStorageService
    {
        void CreateStorage(StorageDTO storage);
        IEnumerable<StorageDTO> GetAllStorages();
        void Dispose();
    }
}
