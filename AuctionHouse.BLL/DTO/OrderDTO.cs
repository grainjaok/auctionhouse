﻿using AuctionHouse.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.BLL.DTO
{
    public class OrderDTO : Order
    {
        public List<int> ProductsId { get; set; }
    }
}
