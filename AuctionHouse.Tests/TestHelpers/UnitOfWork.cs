﻿using AuctionHouse.Domen.Entities;
using AuctionHouse.Domen.Interfaces;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionHouse.Tests.TestHelpers
{
    public class UnitOfWork
    {
        public static IUnitOfWork MockUnitOfWork(List<Product> products)
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();

            mockUnitOfWork.Setup(x => x.Products.GetAll()).Returns(products.AsQueryable());
            mockUnitOfWork.Setup(x => x.Products.Create(It.IsAny<Product>())).Callback<Product>((s) => products.Add(s)); ;

            return mockUnitOfWork.Object;
        }
    }
}


