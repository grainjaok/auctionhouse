﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AuctionHouse.API.Controllers;
using Moq;
using AuctionHouse.Domen.Interfaces;
using System.Net.Http;
using AuctionHouse.Domen.Entities;
using AuctionHouse.BLL.Services;
using AuctionHouse.Tests.TestHelpers;

using System.Linq;
using System.Collections.Generic;

namespace AuctionHouse.Tests
{
    [TestClass]
    public class ProductServiceTest
    {
        private IUnitOfWork unitOfWork;

        [TestMethod]
        public void GetAllProducts_AddNewOne_CountNumber()
        {
            var products1 = new Product();
            var products2 = new Product();
            List<Product> listproduct = new List<Product> { products1, products2 };
            unitOfWork = UnitOfWork.MockUnitOfWork(listproduct);

            var productsCount = unitOfWork.Products.GetAll().Count();
            var productService = new ProductService(unitOfWork);
            // Act
            productService.CreateProduct(listproduct.FirstOrDefault());
                       
            // Assert
           Assert.AreEqual(productsCount + 1, unitOfWork.Products.GetAll().Count());
        }
    }
}
