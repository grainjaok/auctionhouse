﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using AuctionHouse.Domen.Interfaces;
using AuctionHouse.Domen.Entities;
using System.Collections.Generic;
using System.Linq;
using AuctionHouse.BLL.Services;

namespace AuctionHouse.Tests
{
    [TestClass]
    public class OrderLineServiceTest
    {
        [TestMethod]
        public void OrderLineCreateObjectGetCountCompareWithValues()
        {

            List<OrderLine> orderLineList = new List<OrderLine> { new OrderLine() };
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.OrderLines.GetAll()).Returns(orderLineList.AsQueryable());
            mockUnitOfWork.Setup(m => m.OrderLines.Create(It.IsAny<OrderLine>())).Callback<OrderLine>((s) => orderLineList.Add(s));
            var ordersCount = mockUnitOfWork.Object.OrderLines.GetAll().Count();
            var orderLineService = new OrderLineService(mockUnitOfWork.Object);
          
            orderLineService.CreateOrderLine(orderLineList.FirstOrDefault());


            Assert.AreEqual(ordersCount+1, mockUnitOfWork.Object.OrderLines.GetAll().Count());
        }
    }
}
